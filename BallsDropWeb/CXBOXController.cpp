#include "CXBOXController.h"

CXBOXController::CXBOXController(int playerNumber)
{
	// Set the Controller Number
	_controllerNum = playerNumber - 1;
	magnitudeL = 0;
	magnitudeR = 0;
}

XINPUT_STATE CXBOXController::GetState()
{
	// Zeroise the state
	ZeroMemory(&_controllerState, sizeof(XINPUT_STATE));

	// Get the state
	XInputGetState(_controllerNum, &_controllerState);

	return _controllerState;
}

bool CXBOXController::IsConnected()
{
	// Zeroise the state
	ZeroMemory(&_controllerState, sizeof(XINPUT_STATE));

	// Get the state
	DWORD Result = XInputGetState(_controllerNum, &_controllerState);

	if(Result == ERROR_SUCCESS)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void CXBOXController::Vibrate(int leftVal, int rightVal)
{
	// Create a Vibraton State
	XINPUT_VIBRATION Vibration;

	// Zeroise the Vibration
	ZeroMemory(&Vibration, sizeof(XINPUT_VIBRATION));

	// Set the Vibration Values
	Vibration.wLeftMotorSpeed = leftVal;
	Vibration.wRightMotorSpeed = rightVal;

	// Vibrate the controller
	XInputSetState(_controllerNum, &Vibration);
}

float CXBOXController::GetLeftMagnitude(void)
{
	magnitudeL = sqrt(this->GetLeftThumbX() * this->GetLeftThumbX() + this->GetLeftThumbY() * this->GetLeftThumbY());

	if (magnitudeL > XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE)
	{
	  //clip the magnitude at its expected maximum value
	  if (magnitudeL > 32767) magnitudeL = 32767;
  
	  //adjust magnitude relative to the end of the dead zone
	  magnitudeL -= XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE;
	}
	else //if the controller is in the deadzone zero out the magnitude
	{
		magnitudeL = 0.0;
	}

	return magnitudeL;
}

float CXBOXController::GetRightMagnitude(void)
{
	magnitudeR = sqrt(this->GetRightThumbX() * this->GetRightThumbX() + this->GetRightThumbY() * this->GetRightThumbY() );

	if (magnitudeR > XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE)
	{
	  //clip the magnitude at its expected maximum value
	  if (magnitudeR > 32767) magnitudeR = 32767;
  
	  //adjust magnitude relative to the end of the dead zone
	  magnitudeR -= XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE;
	}
	else //if the controller is in the deadzone zero out the magnitude
	{
		magnitudeR = 0.0;
	}

	return magnitudeR;
}

float CXBOXController::GetLeftThumbX(void)
{
	return this->GetState().Gamepad.sThumbLX;
}

float CXBOXController::GetLeftThumbY(void)
{
	return this->GetState().Gamepad.sThumbLY;
}

float CXBOXController::GetRightThumbX(void)
{
	return this->GetState().Gamepad.sThumbRX;
}

float CXBOXController::GetRightThumbY(void)
{
	return this->GetState().Gamepad.sThumbRY;
}