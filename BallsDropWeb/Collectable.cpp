#include "Collectable.h"


Collectable::Collectable(glm::vec3 iPosition,glm::vec3 direction, ShaderManager shaderManager)
{
	this->SetUpFrame(iPosition, direction);
	this->shader = shaderManager.COLOR_ONLY;
}

Collectable::~Collectable(void)
{

}

void Collectable::SetUpModel(char *fileName)
{
	model = new OBJ_Model();
	model->Load(fileName);
	vertices = model->GetVertices();
	normals = model->GetNormals();
	texCoords = model->GetTexCoords();
	radius = 0.0f;
	for(GLuint i = 0; i < vertices.size(); i++)
	{ 
		if(glm::abs(vertices[i].x) > dimensions.x)			dimensions.x = glm::abs(vertices[i].x);
		if(glm::abs(vertices[i].y) > dimensions.y)			dimensions.y = glm::abs(vertices[i].y);
		if(glm::abs(vertices[i].z) > dimensions.z)			dimensions.z = glm::abs(vertices[i].z);
	}
	radius = glm::length( dimensions ) * 4.5;
	std::cout << "collision shpere "<< radius << " units" << std::endl;
	delete model;
}

void Collectable::SetUpFrame(glm::vec3 iPosition, glm::vec3 direction)
{
	frame = new Frame();
	frame->SetOrigin(iPosition );
	frame->SetForwardVector(direction);
	//frame->TranslateLocal( -dimensions.x, -dimensions.y, -dimensions.z );
}

void Collectable::SetUpVBOs(  char *fileName)
{
	//glGenVertexArrays(1, &vao); // Allocate & assign Vertex Array Object (VAO)
	//glBindVertexArray(vao); // Bind VAO as current object

	/*
		really only want to do this once per init stage
		avoid creating buffers for each object included!
		WebGL means we can't use VAOs, annoying
	*/
	SetUpModel(fileName);
	glGenBuffers(2, vbo);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); //Bind the vertex buffer
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * vertices.size(), &vertices[0], GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);

	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); //Bind the normal buffer
	glBufferData(GL_ARRAY_BUFFER, normals.size() * sizeof(glm::vec3), &normals[0] , GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_COLOR);
}

bool Collectable::CollidesWith(Ball *ball)
{
	float distX = glm::abs(frame->GetOriginX() - ball->GetPosition().x);
	float distY = glm::abs(frame->GetOriginY() - ball->GetPosition().y);
	float distZ = glm::abs(frame->GetOriginZ() - ball->GetPosition().z);
	/*distX -= radius;// what is collectables radius?
	distY -= radius;
	distZ -= radius;*/
	
	//one line of code doing too much!
	if ( glm::abs((distX * distX) + (distY * distY) + (distZ * distZ)) <= 
		(  (glm::abs(ball->getRadius()) + glm::abs(radius)) * (glm::abs( radius ) + glm::abs(ball->getRadius()))  ) )
	{
		return true;
	}
	return false;
}

void Collectable::PreRender() {
	//glBindVertexArray(vao);
	//MUST be called by parent object for every frame
	//SetUpVBOs(); //no vao in WebGL, currently
	glUseProgram(shader);

	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); //Bind the vertex buffer
	glVertexAttribPointer(ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	
	glEnableVertexAttribArray(ATTRIBUTE_COLOR);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); //Bind the normal buffer
	glVertexAttribPointer(ATTRIBUTE_COLOR, 3, GL_FLOAT, GL_FALSE, 0, 0);
}

void Collectable::Render(MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME)
{
	glm::mat4 matrix(1.0);

	modelviewStack.push(matrix);
	matrix = frame->GetMatrix();
	modelviewStack.multiplyMatrix(matrix);

	int uniformIndex = glGetUniformLocation(shader, "projection");
	glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(projectionStack.getMatrix()));
	uniformIndex = glGetUniformLocation(shader, "modelview");
	glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(modelviewStack.getMatrix()));
		
	int vertCount = vertices.size();
	if(vertCount == 0) { vertCount = 192;} //a hack to get this rendering multiple copies of this model with one set of buffers.
	//ie verts might be empty but the buffer is set up.

	if (WIRE_FRAME)
	{
		glDrawArrays(GL_LINES, 0, vertCount);
	}
	else
	{
		glDrawArrays(GL_TRIANGLES, 0, vertCount);
	}

	//only switch these off if we have rendered all arrows this frame
	//glDisableVertexAttribArray(ATTRIBUTE_VERTEX);
	//glDisableVertexAttribArray(ATTRIBUTE_COLOR);

	modelviewStack.pop();

	//glUseProgram(0);
	//glBindVertexArray(0);
}

void Collectable::ClearBuffer() {
	glDeleteBuffers(2, vbo);
}

void Collectable::Cleanup(void)
{
	delete frame;
}