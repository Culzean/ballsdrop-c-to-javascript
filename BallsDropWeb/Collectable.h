#pragma once

#ifndef COLLECTABLE_H
#define COLLECTABLE_H

#include "OBJ_Model.h"
#include "stafx.h"

class Collectable
{

public:
	Collectable(glm::vec3 iPosition, glm::vec3 direction, ShaderManager shaderManager);
	~Collectable(void);
	bool CollidesWith(Ball *ball);
	void SetUpVBOs( char *fileName);
	void PreRender();
	void Render(MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME);
	void Cleanup(void);
	void ClearBuffer(); 

private:
	void SetUpModel(char *fileName);
	void SetUpFrame(glm::vec3 iPosition, glm::vec3 direction);
	
	OBJ_Model *model;
	GLfloat	radius;
	glm::vec3 dimensions;
	Frame *frame;
	GLuint vao;
	GLuint vbo[3];
	GLuint shader;
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texCoords;
};


#endif