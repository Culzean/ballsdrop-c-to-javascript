#ifdef _MSC_VER
	#pragma once
#endif

#ifndef FRAME_H
#define FRAME_H


#include <glm/glm.hpp>
#include <glm/gtc\matrix_transform.hpp>
#include <glm/gtc\type_ptr.hpp>
#include <glm/gtc\matrix_access.hpp>
#include <iostream>

class Frame{

protected:
	glm::vec3 vOrigin;
	glm::vec3 vForward;
	glm::vec3 vUp;
	glm::mat4 matrix;
	float xRot;
	float yRot;
	

public:
	float zRot;

	Frame(void){
		vOrigin = glm::vec3(0.0f, 0.0f, 0.0f); 
		vUp = glm::vec3(0.0f, 1.0f, 0.0f);
		vForward = glm::vec3(0.0f, 0.0f, -1.0f);
		matrix = glm::mat4(1.);
		xRot = 0;
		yRot = 0;
		zRot = 0;
    }

	void SetOrigin(glm::vec3 vPoint) { vOrigin = vPoint; }
	glm::vec3 GetOrigin() { return vOrigin; }
	float GetOriginX(void) { return vOrigin.x; }
	float GetOriginY(void) { return vOrigin.y; }
	float GetOriginZ(void) { return vOrigin.z; }

	void SetForwardVector(glm::vec3 vDirection) { vForward = vDirection; }
	glm::vec3 GetForwardVector()				{ return vForward; }

	void SetUpVector(glm::vec3 vDirection) { vUp = vDirection; }
	void GetUpVector(glm::vec3 vVector) { vVector = vUp; }

//??	void GetZAxis(glm::vec3 vVector) { GetForwardVector(vVector); }
	void GetYAxis(glm::vec3 vVector) { GetUpVector(vVector); }
	void GetXAxis(glm::vec3 vVector) { vVector = glm::cross(vUp, vForward); }

	void TranslateWorld(float x, float y, float z)
	{ vOrigin.x += x; vOrigin.y += y; vOrigin.z += z; }

    void TranslateLocal(float x, float y, float z)
	{ MoveForward(z); MoveUp(y); MoveRight(x);	}

	void MoveForward(float fDelta){
		vOrigin.x += vForward.x * fDelta;
		vOrigin.y += vForward.y * fDelta;
		vOrigin.z += vForward.z * fDelta;
	}
	void MoveUp(float fDelta){
		vOrigin.x += vUp.x * fDelta;
		vOrigin.y += vUp.y * fDelta;
		vOrigin.z += vUp.z * fDelta;
	}
	void MoveRight(float fDelta){
		glm::vec3 vCross;
		vCross = glm::cross(vUp, vForward);

		vOrigin.x += vCross.x * fDelta;
		vOrigin.y += vCross.y * fDelta;
		vOrigin.z += vCross.z * fDelta;
	}

	void CalcMatrix(){
		glm::vec3 vXAxis;
		vXAxis = glm::cross(vUp, vForward);

		matrix = glm::mat4( vXAxis.x,		vXAxis.y,		vXAxis.z,		0, 
							vForward.x,		vForward.y,		vForward.z,		0,
							vUp.x,			vUp.y,			vUp.z,			0, 
							vOrigin.x,		vOrigin.y,		vOrigin.z,		1);
	}

	glm::mat4 RotateLocalX(float alpha)
	{
		xRot += alpha;
		matrix = glm::rotate(matrix, xRot, glm::vec3(1.0f, 0.0f, 0.0f));
		return matrix;
	}

	glm::mat4 RotateLocalY(float alpha)
	{
		yRot += alpha;
		matrix = glm::rotate(matrix, yRot, glm::vec3(0.0f, 1.0f, 0.0f));
		return matrix;
	}

	glm::mat4 RotateLocalZ(float alpha)
	{
		zRot += alpha;
		matrix = glm::rotate(matrix, zRot, glm::vec3(0.0f, 0.0f, 1.0f));
		return matrix;
	}

	glm::mat4 GetMatrix(void)
	{
		CalcMatrix();
		return matrix;
	}

	void GetCameraMatrix(glm::mat4 m, bool bRotationOnly = false){
        glm::vec3 x, z;

		z.x = -vForward.x;
		z.y = -vForward.y;
		z.z = -vForward.z;

		x = glm::cross(vUp, z);

		glm::row(m, 0, glm::vec4(x, 0.0f));
		glm::row(m, 1, glm::vec4(vUp, 0.0f));
		glm::row(m, 2, glm::vec4(z, 0.0f));
		glm::row(m, 3, glm::vec4(0.0f, 0.0f, 0.0f, 1.0f));
			
        if(bRotationOnly)
            return;
                
        // Apply translation too
        glm::mat4 trans, M;
		glm::translate(trans, -vOrigin);
		M = m * trans;

        // Copy result back into m
        m = M;
	}
};

#endif