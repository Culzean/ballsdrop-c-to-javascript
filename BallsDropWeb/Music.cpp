#include "Music.h"

void Music::loadSound(char *filename){

	music = Mix_LoadMUS(filename);
	if(!music) {
		std::cout << "Troubled set up music file : " << filename << std::endl;
	}
	/*if (!music.openFromFile(filename))
	{
		//Error
	}*/
}

void Music::play()
{
	channel = Mix_PlayMusic(music, looped);
	//music.play();
}

void Music::stop()
{
	Mix_HaltChannel(channel);
	//music.stop();
}

void Music::SetLoop(bool loop)
{
	looped = loop;
}