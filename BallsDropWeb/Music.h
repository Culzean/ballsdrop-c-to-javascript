#include <iostream>
#include <SDL/SDL_mixer.h>

#ifdef _MSC_VER
	#pragma once
#endif

#ifndef MUSIC_H
#define MUSIC_H

class Music{

public:
	void loadSound(char *filename);
	void play();
	void stop();
	int GetStatus(void) { return Mix_Playing(channel); }
	//sf::Music::Status GetStatus(void) { return music.getStatus(); }
	void SetLoop(bool loop);

private:
	Mix_Music* music;
	int channel; //last channel this was associated with
	bool looped; //shoudl this sound be looped?
	//sf::Music music;
};

#endif