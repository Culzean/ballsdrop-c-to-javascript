#include "OBJ_Model.h"
#include "stafx.h"



OBJ_Model::OBJ_Model(void)
{
}


OBJ_Model::~OBJ_Model(void)
{
	vertices.clear();
	normals.clear();
	texCoords.clear();
}

void OBJ_Model::ClearVectors(void)
{
	temp_vertices.clear();
	vertices.clear();
	vertexIndices.clear();
}


bool OBJ_Model::LoadPointList(char* fileName)
{
	std::string temp;

	std::stringstream objFile;
	std::ifstream fileTmp(fileName);

	if(fileTmp.is_open())
	{
		objFile << fileTmp.rdbuf();
		fileTmp.close();
	}
	else
	{
		std::cout << "File not found!" << std::endl;
		return false;
	}

	GLfloat result;

	while (objFile >> temp)					// Start reading file data
	{		
		if(temp == "v")   //if the next line is a vertex
		{
			glm::vec3 vertex;  ///create a temp storage of type vec3
			for(int i = 0; i < 3 ;i++)       //loop for the three data items in the line
			{
				objFile >> result;    //pasre the data into a temp variable
				if(i==0)              //if i = 0 store the data as the x coordinate of the vertex
					vertex.x = result;
				else if(i==1)		  //if i = 1 store the data as the y coordinate of the vertex
					vertex.y = result;
				else vertex.z = result; //else stor it as the z coord 
			}

			temp_vertices.push_back(vertex); //push the vec 3 into the temp vector for vertices

		}


		if(temp == "f")
		{
			unsigned int vertexIndex[4]; //create temp Indices for the vertices, textures and normals

			for(int i = 0; i < 4; i++) //loop for the 3 faces in the line
			{
				objFile >> vertexIndex[i]; //parse the first index as the vertex

			}

			vertexIndices.push_back(vertexIndex[0]); //push the first vertex index into the vertex Indices list
			vertexIndices.push_back(vertexIndex[1]); //push the second vertex index into the vertex Indices list
			vertexIndices.push_back(vertexIndex[2]); //push the third vertex index into the vertex Indices list
			vertexIndices.push_back(vertexIndex[3]); //push the fourth vertex index into the vertex Indices list
		}

	}

	unsigned int vertexIndex = 0;
	if (vertexIndices.size() > 0)
	{
		// For each vertex of each quad
		for( unsigned int i=0; i < (vertexIndices.size()) ; i++)
		{
			vertexIndex = vertexIndices[i]; //store the current index

			glm::vec3 vertex = temp_vertices[ vertexIndex-1 ]; //copy the vertex in the index (-1 because index starts at 1 not 0)

			vertices.push_back(vertex); //push the right vertex in the real vertices vector

			//std::cout << vertices[i].x << " " << vertices[i].y << " " << vertices[i].z << "\n";
		}
	}


	return true;
}


bool OBJ_Model::Load(char* fileName)
{
	std::string temp;  //Used to store the next input
	//std::ifstream objFile (fname);	//opens the file
	std::stringstream objFile;
	std::ifstream fileTmp(fileName);

	if (fileTmp.is_open())
	{
		objFile << fileTmp.rdbuf();
		fileTmp.close();
	}
	else
	{
		std::cout << "File not found!" << std::endl;
		return false;
	}

	GLfloat result;  //used to temp store the float numbers

	while (objFile >> temp)					// Start reading file data
	{		
		if(temp == "v")   //if the next line is a vertex
		{
			glm::vec3 vertex;  ///create a temp storage of type vec3
			for(int i = 0; i < 3 ;i++)       //loop for the three data items in the line
			{
				objFile >> result;    //pasre the data into a temp variable
				if(i==0)              //if i = 0 store the data as the x coordinate of the vertex
					vertex.x = result;
				else if(i==1)		  //if i = 1 store the data as the y coordinate of the vertex
					vertex.y = result;
				else vertex.z = result; //else stor it as the z coord 
			}

			temp_vertices.push_back(vertex); //push the vec 3 into the temp vector for vertices

		}

		if(temp== "vt") //if the next line is texture coordinates
		{
			glm::vec2 tex; ///create a temp storage of type vec3

			for(int i = 0; i < 2 ;i++) //loop for the two coordiantes in the line
			{
				objFile >> result; //parse data into the temp variable
				if(i==0)    //if i = 0 store as the u coordinate of the texture
					tex.x = result;
				else        //else store as the v coordinate of the texture
					tex.y = result;
			}

			temp_tex.push_back(tex); //push the vec 2 into the temp vector for texture coords
		}


		if(temp == "vn") // if the next line is a normal
		{
			glm::vec3 normal; ///create a temp storage of type vec3

			for(int i = 0; i < 3; i++)   // loop for the 3 coordinates in the line
			{
				objFile>> result; //parse data into the temp variable

				if(i==0) //if i= 0 store as the normals x coordinate
					normal.x = result;
				else if(i==1) //if i= 1 store as the normals y coordinate
					normal.y = result;
				else normal.z = result; //else store as the normals z coordinate
			}

			temp_normals.push_back(normal); //push vec 3 into the temp vector for normals
		}


		if(temp == "f")
		{
			unsigned int vertexIndex[3], texIndex[3], normalIndex[3]; //create temp Indices for the vertices, textures and normals

			for(int i = 0; i < 3; i++) //loop for the 3 faces in the line
			{
				objFile >> vertexIndex[i]; //parse the first index as the vertex
				if(objFile.get() == '/') //Extracts the character from the stream and returns its value
				{
					if (objFile.peek() != '/') //if the value of the next character is not a '/'.
					{
						objFile >> texIndex[i]; // parse the second index as the texture index

					}
					if (objFile.get() == '/') // Extracts the character from the stream and returns its value
					{
						objFile >> normalIndex[i]; // parse the third index as the normal index
					}
				}

			}

			vertexIndices.push_back(vertexIndex[0]); //push the first vertex index into the vertex Indices list
			vertexIndices.push_back(vertexIndex[1]); //push the second vertex index into the vertex Indices list
			vertexIndices.push_back(vertexIndex[2]); //push the third vertex index into the vertex Indices list
			texIndices    .push_back(texIndex[0]);   //push the first texture index into the texture index list
			texIndices    .push_back(texIndex[1]);	//push the seconed texture index into the texture index list
			texIndices    .push_back(texIndex[2]);	//push the third texture index into the texture index list
			normalIndices.push_back(normalIndex[0]); //push the first normal index into the normal index list
			normalIndices.push_back(normalIndex[1]); //push the seconed normal index into the normal index list
			normalIndices.push_back(normalIndex[2]); //push the third normal index into the normal index list

		}


	}

	unsigned int vertexIndex = 0;
	if (vertexIndices.size() > 0)
	{
		// For each vertex of each triangle
		for( unsigned int i=0; i < (vertexIndices.size()) ; i++)
		{
			vertexIndex = vertexIndices[i]; //store the current index

			glm::vec3 vertex = temp_vertices[ vertexIndex-1 ]; //copy the vertex in the index (-1 because index starts at 1 not 0)

			vertices.push_back(vertex); //push the right vertex in the real vertices vector
		}
	}


	unsigned int texIndex = 0;
	// For each texture coordinate of each triangle
	for( unsigned int i=0; i < (texIndices.size()) ; i++)
	{
		texIndex = texIndices[i]; //store the current index

		glm::vec2 texCoord = temp_tex[ texIndex -1]; //copy the tex coord in the index (-1 because index starts at 1 not 0)

		texCoords.push_back(texCoord); //push the right tex coord in the real tex coord vector
	}

	unsigned int normalIndex = 0;
	// For each normal of each triangle
	for( unsigned int i=0; i < (normalIndices.size()) ; i++)
	{
		normalIndex = normalIndices[i]; //store the current index

		glm::vec3 normal = temp_normals[ normalIndex-1 ]; //copy the normal in the index (-1 because index starts at 1 not 0)

		normals.push_back(normal); //push the right normal in the real normal vector
	}

	vertexIndices.clear();
	texIndices.clear();
	normalIndices.clear();
	temp_vertices.clear();
	temp_tex.clear();
	temp_normals.clear();

	return true;
}