#include "Options.h"

Options::Options()
{
	EXIT = false;
	selection = 0;
	musicSelection = 0;
	subMenu = false;
	repeatTimer = 0.0f;
}

Options::~Options()
{
}

void Options::Init(WindowManager windowManager, ShaderManager shaderManager, int musicSelection)
{
	EXIT = false;
	selection = 0;
	musicSelection = 0;
	subMenu = false;
	repeatTimer = 0.0f;
	projectionStack.loadMatrix(glm::perspective(45.0f, 4.0f / 3.0f, 0.5f, 1000.f));
	//soundBank = SoundBank::GetInstance();
	SDL_Color white = {255, 255, 255};
	SDL_Color blue = {0, 0, 100};
	music = new TextArea(glm::vec3(0.5, 1.2, 3.0), "Verdana", "MUSIC", white, shaderManager);
	rock = new TextArea(glm::vec3(-2.0, 0.5, 1.5), "Verdana", "ROCK", white, shaderManager);
	rock->SetAlpha(0.6f);
	electro = new TextArea(glm::vec3(0.5, 0.5, 1.5), "Verdana", "ELECTRO", white, shaderManager);
	electro->SetAlpha(0.6f);
	weird = new TextArea(glm::vec3(3.0, 0.5, 1.5), "Verdana", "JAUNTY", white, shaderManager);
	weird->SetAlpha(0.6f);
	theme = new TextArea(glm::vec3(0.5, -1.0, 3.0), "Verdana", "THEME", white, shaderManager);
}

void Options::Update(GLfloat dt, float fps)
{
	SDL_Color blue = {0, 0, 100};
	SDL_Color white = {255, 255, 255};

	repeatTimer += dt;
	if(repeatTimer > TIME_REPEAT_KEY)
	{
		repeat = true;
		repeatTimer = 0.0f;
	}

	if (selection == -3)
	{
		music->SetColour(blue);
		music->SetZ(3.5);
	}
	else
	{
		music->SetColour(white);
		music->SetZ(3.0);
	}

	if (selection == -4)
	{
		theme->SetColour(blue);
		theme->SetZ(4.5);
	}
	else
	{
		theme->SetColour(white);
		theme->SetZ(3.0);
	}

	if (musicSelection == 0)
	{
		rock->SetColour(blue);
	}
	else
	{
		rock->SetColour(white);
	}

	if (musicSelection == 1)
	{
		electro->SetColour(blue);
	}
	else
	{
		electro->SetColour(white);
	}

	if (musicSelection == 2)
	{
		weird->SetColour(blue);
	}
	else
	{
		weird->SetColour(white);
	}
}
/*
void Options::GetInput(CXBOXController *Player1)
{
	if (Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_UP 
		&& prevState.Gamepad.wButtons != XINPUT_GAMEPAD_DPAD_UP && (selection + 1) != -2)
	{
		selection += 1;
		soundBank->navigate.play();
	}

	if (Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_DOWN 
		&& prevState.Gamepad.wButtons != XINPUT_GAMEPAD_DPAD_DOWN && (selection - 1) != -5 && subMenu)
	{
		selection -= 1;
		soundBank->navigate.play();
	}

	if (selection == -3)
	{
		if (Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_RIGHT 
		&& prevState.Gamepad.wButtons != XINPUT_GAMEPAD_DPAD_RIGHT && (musicSelection + 1) != 3)
		{
			musicSelection += 1;
			soundBank->navigate.play();
		}

		if (Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_DPAD_LEFT 
		&& prevState.Gamepad.wButtons != XINPUT_GAMEPAD_DPAD_LEFT && (musicSelection - 1) != -1)
		{
			musicSelection -= 1;
			soundBank->navigate.play();
		}

		if (Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_A 
		&& prevState.Gamepad.wButtons != XINPUT_GAMEPAD_A)
		{
			if (!subMenu)
			{
				subMenu = true;
				rock->SetAlpha(0.0f);
				electro->SetAlpha(0.0f);
				weird->SetAlpha(0.0f);
			}
			else
			{
				subMenu = false;
				rock->SetAlpha(0.6f);
				electro->SetAlpha(0.6f);
				weird->SetAlpha(0.6f);
			}
		}
	}

	prevState = Player1->GetState();
}
*/

void Options::KeyHandler( SDL_Event sdlEvent )
{
	if(sdlEvent.type == SDL_KEYDOWN)
	{
		switch(sdlEvent.key.keysym.sym)
		{
			case SDLK_UP:
				if((selection + 1) != -2 && repeat){
					selection += 1;
				//soundBank->navigate.play();
				}
				repeat = false;
			break;
			case SDLK_DOWN:
				if((selection - 1) != -5 && repeat && subMenu){
					selection -= 1;
				//soundBank->navigate.play();
				}
				repeat = false;
			break;
			case SDLK_LEFT:

				if (selection == -3)
				{
					if ( repeat && (musicSelection - 1) != -1)
					{
						musicSelection -= 1;
						//soundBank->navigate.play();
					}
				}
				repeat = false;
			break;
			case SDLK_RIGHT:
			if (selection == -3)
			{
					if (repeat && (musicSelection + 1) != 3)
					{
						musicSelection += 1;
						//soundBank->navigate.play();
					}
			}
			repeat = false;
			break;
			case SDLK_RETURN:
				
			if ( repeat)
			{
			if (!subMenu)
				{
					subMenu = true;
					rock->SetAlpha(0.0f);
					electro->SetAlpha(0.0f);
					weird->SetAlpha(0.0f);
				}
				else
				{
					subMenu = false;
					rock->SetAlpha(0.6f);
					electro->SetAlpha(0.6f);
					weird->SetAlpha(0.6f);
				}
			}
			repeat = false;
			break;

			case SDLK_ESCAPE:

				if(repeat) {
					EXIT = true;
					repeat = false;
				}
				break;

			case SDLK_q:

				if(repeat) {
					EXIT = true;
					repeat = false;
				}
				break;

			default:
				break;
		}		
	}
}

void Options::Render(bool WIRE_FRAME)
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	if (WIRE_FRAME)
		glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	else
		glClearColor(0.0f, 0.8f, 0.4f, 1.0f);

	modelviewStack.loadIdentity();
	modelviewStack.push(); 

	music->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
	rock->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
	electro->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
	weird->Render(projectionStack, modelviewStack, WIRE_FRAME, true);
	theme->Render(projectionStack, modelviewStack, WIRE_FRAME, true);

	modelviewStack.pop();
}

void Options::Cleanup(void)
{
	delete music;
	delete rock;
	delete electro;
	delete weird;
	delete theme;
}