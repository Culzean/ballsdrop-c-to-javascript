#include "PhysicsManager.h"

PhysicsManager::PhysicsManager() {

}

PhysicsManager::~PhysicsManager() {

}

GLfloat PhysicsManager::BallVsPlane( Ball* b0, Plane* p0 ) {

	//find distance to this plane
	//if dist vlaue is less than zero 
	//then add the negative of dist to pos

	//test where we can going to be
	GLfloat dist = distanceToPlane( ( b0->GetPosition() - p0->getNormal() * b0->getRadius() ), p0 );

	if(dist <= 0 && !b0->isOutOfBounds())
	{
		glm::vec3 col = glm::vec3( p0->getNormal() * -dist );
		b0->SetPosition(b0->GetPosition() + col);
		b0->setOnGround(true);
		//std::cout << "collision trig : " << -dist << std::endl;
	} else {
		b0->setOnGround(false);
		//std::cout << "not getting back to the races: " << dist << " bounds: " << b0->isOutOfBounds() << std::endl;
	}
	return dist;
}

GLfloat PhysicsManager::distanceToPlane( glm::vec3 point, Plane* const plane ) {

	//using the plane normal, the position of the ball and geometric construct D(formular of a plane)
	//we will check the distance form the ball to the plane
	glm::vec3 N = plane->getNormal();
	GLfloat D = glm::dot((-N), plane->getPos());

	return glm::dot(N, point) + D;
}

glm::vec3 PhysicsManager::projOnPlane( glm::vec3& vec, Plane* const plane ) {

	//input a vector and a plane
	//find the vector as seen on a plane
	//note that if distance == 0 then the vector unaltered
	glm::vec3 V(plane->getPos() + vec);
	
	GLfloat c = distanceToPlane( V, plane );
	glm::vec3 cN = c * plane->getNormal();

	return (vec - cN);
}

//find the correct section of track to test for more collisions
bool PhysicsManager::findTrackSect( TrackSect* _sect, glm::vec3 obPos, GLfloat obRad ) {

	GLfloat diffX = (_sect->getWidth()) * 0.5f;
	GLfloat localPosX = glm::abs(glm::abs(obPos.x) + obRad) - glm::abs(_sect->getPos().x);

	if( glm::abs(diffX) >= glm::abs(localPosX) )
	{
		//test on z
		GLfloat diffZ = (_sect->getDepth()) * 0.5f;
		GLfloat localPosZ = glm::abs(glm::abs(obPos.z) + obRad) - glm::abs(_sect->getPos().z);
		if( glm::abs(diffZ) >= glm::abs(localPosZ) )
		{
			GLfloat diffY = (_sect->getHeight() * 0.5f);
			GLfloat localPosY = glm::abs(glm::abs(obPos.y) + obRad) - glm::abs(_sect->getPos().y);
			if(	glm::abs(diffY) >= glm::abs(localPosY))
				return true;
		}
	}
	return false;
}


/*
	The definitive reference is "Point in Polygon Strategies" by
	Eric Haines [Gems IV]  pp. 24-46.
	The essence of the ray-crossing method is as follows.
	Think of standing inside a field with a fence representing the polygon.
	Then walk north. If you have to jump the fence you know you are now
	outside the poly. If you have to cross again you know you are now
	inside again; i.e., if you were inside the field to start with, the total
	number of fence jumps you would make will be odd, whereas if you were
	outside the jumps will be even.
	The code below is from Wm. Randolph Franklin <wrf@ecse.rpi.edu>
	with some minor modifications for speed.  It returns 1 for strictly
	interior points, 0 for strictly exterior, and 0 or 1 for points on
	the boundary.  The boundary behaviour is complex but determined;
	in particular, for a partition of a region into polygons, each point
	is "in" exactly one polygon. 
   
	The code may be further accelerated, at some loss in clarity, by
	avoiding the central computation when the inequality can be deduced,
	and by replacing the division by a multiplication for those processors
	with slow divides.

	numPoints = number of points
	poly = array of vectors representing each point
	x,y = point to test
*/

bool PhysicsManager::pnpoly(int numPoints, glm::vec3* poly, float x, float z)
{
	//this is a ray casting test
	//the value c counts the number of times that the ray strikes an edge
	//if this is odd then the point we are following is within the triangle
		int i, j, c = 0;     

	   for (i = 0, j = numPoints-1; i < numPoints; j = i++)
	   {
			  if ((((poly[i].z<=z) && (z<poly[j].z)) || ((poly[j].z<=z) && (z<poly[i].z))) &&
				   (x < (poly[j].x - poly[i].x) * (z - poly[i].z) / (poly[j].z - poly[i].z) + poly[i].x))
			  c = !c;     
	   }     

	   return (c==1);   //c counts the 
}


bool PhysicsManager::BallVsBall( Ball* b0, Ball* b1 )
{
	glm::vec3 dist = glm::abs( b0->GetPosition() - b1->GetPosition() );
	GLfloat scalar = ( (dist[0] * dist[0]) + (dist[1] * dist[1]) + (dist[2] * dist[2]) );
	GLfloat radDist = b0->getRadius() * b0->getRadius() + b1->getRadius() * b1->getRadius();

	if( scalar <= radDist )
	{
		return true;
	}
	return false;
}