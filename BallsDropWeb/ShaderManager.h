#ifdef _MSC_VER
	#pragma once
#endif

#ifndef SHADERMANAGER_H
#define SHADERMANAGER_H

#include <GL/glew.h>
#include <iostream>
#include <fstream>
#include <map>
#include <string>

//list of attributes to give vbos more meaningful names
//problem allining these enums to a limited number of vbos
enum AGP_ATTRIBUTE { ATTRIBUTE_VERTEX = 0, ATTRIBUTE_COLOR, ATTRIBUTE_NORMAL, ATTRIBUTE_INDICES,
	ATTRIBUTE_TEXTURE0, ATTRIBUTE_TEXTURE1, ATTRIBUTE_TEXTURE2, ATTRIBUTE_VELOCITY, ATTRIBUTE_ACCELERATION };

class ShaderManager{

	public:
		void InitShaders(void);
		GLuint LoadShader(char *vertFile, char *fragFile);
		char* LoadFile(char *fileName);
		void PrintShaderInfoLog(GLint shader);
		void ClearShaders(void);

		GLuint COLOR_ONLY;
		GLuint TOON;
		GLuint SKYBOX;
		GLuint PARTICLE;
		GLuint TEXTURE;
		GLuint GRIND;
		GLuint TEXTURE_ONLY;

	private:
		std::map<std::string, GLuint>				shaderTable;
		std::map<std::string , GLuint>::iterator	itr;
};

#endif