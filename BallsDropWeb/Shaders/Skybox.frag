// cubeMap.frag cubeMap fragment shader
#version 100
#ifdef GL_ES
precision mediump float;
#endif
#ifndef GL_ES
precision highp float;
#endif

//out vec4 gl_FragColor;
varying vec3 cubeTexCoord;
uniform samplerCube cubeMap;

void main(void) {   
	// Fragment colour
	gl_FragColor = textureCube(cubeMap, cubeTexCoord);
}
