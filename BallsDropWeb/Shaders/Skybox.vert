// cubeMap.vert
// Vertex shader for cubemap for e.g. sky box, with no lights
#version 100
#ifdef GL_ES
precision mediump float;
#endif
#ifndef GL_ES
precision highp float;
#endif

uniform mat4 modelview;
uniform mat4 projection;

attribute vec3 in_Position;
varying vec3 cubeTexCoord;

void main(void) {
	// vertex into eye coordinates
	gl_Position = projection * modelview * vec4(in_Position,1.0);
	cubeTexCoord = normalize(in_Position);
}
