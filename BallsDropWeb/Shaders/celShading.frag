// simple fragment shader simple.frag
//set up for WebGL
#version 100
#ifdef GL_ES
precision mediump float;
#endif

//uniform sampler1D texture0;
uniform sampler2D texture0;
uniform sampler2D texMap;

varying   vec3 ex_Color;
varying   vec3 ex_N;
varying   vec3 ex_V;
varying   vec3 ex_L;
varying   vec2 ex_TexCoord;
varying   float ONED_Coord;
//varying   vec2 ONED_Coord;

// GLSL versions after 1.3 remove the built in type gl_FragColor
// If using a shader lang version greater than #version 130
// you *may* need to uncomment the following line:
// out vec4 gl_FragColor
 
void main(void) {
	// Fragment colour
	if(ex_Color.xyz == vec3(0.0))
		gl_FragColor = vec4(vec3(0.0), 1.0);
	else
	{
		gl_FragColor = vec4( texture2D(texMap,ex_TexCoord) * texture2D(texture0, vec2(ONED_Coord), 1.0 ) );
		//gl_FragColor = vec4(ex_TexCoord, 0.0f, 1.0f);
		//gl_FragColor = texture(texture0, ONED_Coord);
	}
}
