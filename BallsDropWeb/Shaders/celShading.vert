// simple vertex shader - simple.vert
#version 100
#ifdef GL_ES
precision mediump float;
#endif
#ifndef GL_ES
precision highp float;
#endif
uniform mat4 modelview;
uniform mat4 projection;
uniform vec4 lightPos;
uniform vec3 cameraPosition;
//uniform sampler1D texture0;
uniform sampler2D texture0;

attribute  vec3 in_Position;
attribute  vec3 in_Color;
attribute  vec3 in_Normal;
attribute  vec2 in_TexCoord;


varying  vec3 ex_Color;
varying  vec3 ex_N;
varying  vec3 ex_V;
varying  vec3 ex_L;
varying  vec2 ex_TexCoord;
varying  float ONED_Coord;

// simple shader program
// multiply each vertex position by the MVP matrix
void main(void) {
	// vertex into eye coordinates
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);
	gl_Position = projection * vertexPosition;

	// Find V - in eye coordinates, eye is at (0,0,0)
	ex_V = normalize(-vertexPosition).xyz;

	// Vertex normal in eye coordinates
	ex_N = normalize(modelview * vec4(in_Normal,0.0)).xyz;

	// L - to light source from vertex
	//going to use the camera as a fixed position
	ex_L = normalize(cameraPosition - vertexPosition.xyz);

	ONED_Coord = max( 0.0 , dot(ex_N, ex_L) );
	ex_TexCoord = in_TexCoord;


	//decide in vertex shader if we want an outline
	//this is a cheap way to make this
	//if( abs(dot(ex_N, ex_V)) < 0.22 )
	//	ex_Color = vec3(0.0);
	//else
		ex_Color = vec3(1.0);
}
