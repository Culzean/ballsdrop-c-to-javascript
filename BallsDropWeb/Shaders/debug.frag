// debug fragment shader
#version 100
#ifdef GL_ES
precision mediump float;
#endif
#ifndef GL_ES
precision highp float;
#endif

 

varying vec3 ex_Color;
attribute vec2 ex_TexCoord;

// GLSL versions after 1.3 remove the built in type gl_FragColor
// If using a shader lang version greater than #version 130
// you *may* need to uncomment the following line:
// out vec4 gl_FragColor
 
void main(void) {

	gl_FragColor = vec4(ex_Color,1.0);
	//

}
