// A debug shader to draw lines
#version 100
#ifdef GL_ES
precision mediump float;
#endif
#ifndef GL_ES
precision highp float;
#endif
uniform mat4 MV;
uniform mat4 projection;

attribute vec3 in_Norm;
//line could be normal, velocity vector, or axis
attribute vec3 in_Color;


varying vec3 ex_Color;

// simple shader program
// multiply each vertex position by the MVP matrix
void main(void) {
    // draw lines in correct MVP pos
	vec4 vertexPosition = MV * vec4(in_Norm,1.0);
	gl_Position = projection * vertexPosition;

	ex_Color = in_Color;

}
