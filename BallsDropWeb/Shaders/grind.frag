// simple fragment shader simple.frag
#version 100
#ifdef GL_ES
precision mediump float;
#endif
#ifndef GL_ES
precision highp float;
#endif

uniform sampler2D texture0;

varying vec4 ex_Color;
varying vec2 ex_TexCoord;

// GLSL versions after 1.3 remove the built in type gl_FragColor
// If using a shader lang version greater than #version 130
// you *may* need to uncomment the following line:
// out vec4 gl_FragColor
 
void main(void) {
	gl_FragColor = ex_Color * texture2D(texture0, gl_PointCoord);
	//gl_FragColor = vec4(gl_PointCoord, 0.0f,1.0f);
	//gl_FragColor = texture(texture0, vec2(1.0));
	//gl_FragColor = ex_Color;
}
