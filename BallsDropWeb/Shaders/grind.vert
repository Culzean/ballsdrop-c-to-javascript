// simple vertex shader - simple.vert
#version 100
#ifdef GL_ES
precision mediump float;
#endif
#ifndef GL_ES
precision highp float;
#endif



uniform mat4 modelview;
uniform mat4 projection;



attribute vec3 in_Position;
attribute vec3 in_Color;
attribute vec2 in_TexCoord;

varying vec4 ex_Color;
varying vec2 ex_TexCoord;

// simple shader program
// particle vertex program
void main(void) {
	
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);
	gl_Position = projection * vertexPosition;

	gl_PointSize = 25.0;

	ex_TexCoord = in_TexCoord;
	ex_Color = vec4(in_Color, 1.0);

}
