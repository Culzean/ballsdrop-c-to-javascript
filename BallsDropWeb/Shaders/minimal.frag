// Fragment Shader � file "minimal.frag"
#version 100
#ifdef GL_ES
precision mediump float;
#endif

varying  vec3 ex_Color;

void main(void)
{
	gl_FragColor = vec4(ex_Color,1.0);
}
