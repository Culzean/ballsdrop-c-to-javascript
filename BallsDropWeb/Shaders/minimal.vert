// Vertex Shader � file "minimal.vert"
#version 100
#ifdef GL_ES
precision mediump float;
#endif



uniform mat4 modelview;
uniform mat4 projection;

attribute  vec3 in_Position;
attribute  vec3 in_Color;

varying vec3 ex_Color;

void main(void)
{
	ex_Color = in_Color;
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);
	gl_Position = projection * vertexPosition;
}