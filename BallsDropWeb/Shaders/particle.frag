// simple fragment shader simple.frag
#version 100
#ifdef GL_ES
precision mediump float;
#endif
#ifndef GL_ES
precision highp float;
#endif

uniform sampler2D texture0;
//uniform sampler1D texture1;
uniform sampler2D texture1;

varying vec4 ex_Color;
varying float ex_TexCoord;

// GLSL versions after 1.3 remove the built in type gl_FragColor
// If using a shader lang version greater than #version 130
// you *may* need to uncomment the following line:
// out vec4 gl_FragColor
 
void main(void) {
	
	if(ex_Color.y == 0.0) {
		discard;
	}

	vec4 fragCol2 = texture2D(texture0, gl_PointCoord);

	//hacked a bit, just to get at the 1d texture data
	gl_FragColor = fragCol2 * texture2D(texture1, vec2(ex_TexCoord, 0.0) ) * ex_Color;
}
