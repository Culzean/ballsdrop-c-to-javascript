// simple vertex shader - simple.vert
#version 100
#ifdef GL_ES
precision mediump float;
#endif
#ifndef GL_ES
precision highp float;
#endif

struct Player {
	vec3 position;
	vec3 velocity;
} player;

//const float MAX_VIEW = 358.0;

uniform mat4 modelview;
uniform mat4 projection;
uniform vec4 lightPos;
uniform float time;
uniform vec4 playerPos;


attribute vec3 in_Position;
attribute vec3 in_Color;
attribute vec2 in_TexCoord;

varying vec4 ex_Color;
varying float ex_TexCoord;

// simple shader program
// particle vertex program
void main(void) {
	
	vec4 vertexPosition = modelview * vec4(in_Position,1.0);
	gl_Position = projection * vertexPosition;

	//determine size for rendering by distance to player
	float viewDist = distance( in_Position, player.position );
	float pointSize = (358.0 / viewDist);

	if(pointSize < 1.0)
		gl_PointSize = 0.0;
	else
		gl_PointSize = pointSize * 5.5;

	ex_Color = vec4(in_Color, 1.0);
	ex_TexCoord = in_TexCoord.x;
}
