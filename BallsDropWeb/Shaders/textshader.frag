#version 100
#ifdef GL_ES
precision mediump float;
#else
precision highp float;
#endif

uniform sampler2D textureUnit0;
uniform bool flipped;
uniform float alpha;
varying vec2 ex_TexCoord;

void main(){
	vec2 flipped_TexCoord;
	if (flipped){
		flipped_TexCoord = vec2(1.0- ex_TexCoord.x, 1.0 - ex_TexCoord.y);
	}
	else{
		flipped_TexCoord = vec2(ex_TexCoord.x, 1.0 - ex_TexCoord.y);
	}
	gl_FragColor = texture2D(textureUnit0, flipped_TexCoord);
	
	if (gl_FragColor.a > 0.0)
	{
		gl_FragColor.a -= (1.0 - alpha);
	}
}