#version 100
#ifdef GL_ES
precision mediump float;
#endif
#ifndef GL_ES
precision highp float;
#endif

uniform mat4 modelview;
uniform mat4 projection;

attribute vec3 in_Position;
attribute vec2 in_TexCoord;

varying vec2 ex_TexCoord;


void main(void){

	vec4 vertexPosition = modelview * vec4(in_Position, 1.0);
	gl_Position = projection * vertexPosition;

	ex_TexCoord = in_Position.xy;
}