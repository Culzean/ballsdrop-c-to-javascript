#version 100
#ifdef GL_ES
precision mediump float;
#endif
#ifndef GL_ES
precision highp float;
#endif

uniform mat4 modelview;
uniform mat4 projection;

attribute vec3 in_Position;
attribute vec3 in_Normal;

varying vec4 ex_Color;
varying vec2 ex_TexCoord;

struct lightStruct{

	vec4 ambient;
	vec4 diffuse;
	vec4 specular;
	vec4 position;
};

struct materialStruct{

vec4 ambient;
vec4 diffuse;
vec4 specular;
float shininess;
};

uniform lightStruct light;
uniform materialStruct material;


void main(void)
{
	vec4 vertexPosition = modelview * vec4(in_Position, 1.0);
	gl_Position = projection * vertexPosition;

	vec3 V = normalize(-vertexPosition.xyz);

	vec3 screenNormal = normalize(modelview * vec4(in_Normal, 0.0)).xyz;

	vec4 ambientI = light.ambient * material.ambient;

	vec3 L = normalize(light.position.xyz - vertexPosition.xyz);

	vec4 diffuseI = light.diffuse * material.diffuse * max(dot(screenNormal, L), 0);

	vec3 R = normalize(-reflect(L, screenNormal));
	
	vec4 specularI = light.specular * material.specular;

	specularI = specularI * pow(max(dot(R,V),0), material.shininess);

	ex_TexCoord = in_Position.xy + vec2(0.5);

	ex_Color = ambientI + diffuseI + specularI;
}