#include "SkyBox.h"

const char *cubeTexFiles[] = {
		"SkyBoxTextures/Skybox-Front.bmp", "SkyBoxTextures/Skybox-Back.bmp", "SkyBoxTextures/Skybox-Right.bmp",
		"SkyBoxTextures/Skybox-Left.bmp", "SkyBoxTextures/Skybox-Top.bmp", "SkyBoxTextures/Skybox-Bottom.bmp" };

SkyBox::SkyBox()
{
}

SkyBox::~SkyBox()
{
	glDeleteBuffers(3, vbo);
}

void SkyBox::Init()
{
	model = new OBJ_Model();
	model->Load("Res/Cube.obj");

	vertices = model->GetVertices();
	normals = model->GetNormals();
	texCoords = model->GetTexCoords();

	delete model;


	//glGenVertexArrays(1, &vao); // Allocate & assign Vertex Array Object (VAO)
	//glBindVertexArray(vao); // Bind VAO as current object

	glGenBuffers(3, vbo); // Allocate three Vertex Buffer Object (VBO)
	
	SetUpVAO();

	//glBindVertexArray(vao);
}

void SkyBox::ApplyTexture(GLuint texture)
{
	this->texture = texture;
}

void SkyBox::AssignShader(GLuint shader)
{
	this->shader = shader;
}

void SkyBox::SetUpVAO() {
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); //Bind 1st VBO as active uffer object
	//Copy the vertex data from model to the VBO
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * vertices.size(), &vertices[0], GL_STATIC_DRAW);
	//glBufferData(GL_ARRAY_BUFFER, numVerts * 3 * sizeof(GLfloat), (GLfloat*)verts, GL_STATIC_DRAW);
	// Position data is going into attribute index 0 & has 3 floats per vertex
	glVertexAttribPointer(ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);  //Enable attribute index 0 (position)


	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); //Bind 2nd VBO as active uffer object
	//Copy the normals from model to the VBO
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3) * normals.size(), &normals[0], GL_STATIC_DRAW);
	//glBufferData(GL_ARRAY_BUFFER, numVerts * 3 * sizeof(GLfloat), (GLfloat*)verts, GL_STATIC_DRAW);
	// Normal data is going into attribute index 1 & has 3 floats per vertex
	glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_NORMAL);  //Enable attribute index 0 (normals)


	glBindBuffer(GL_ARRAY_BUFFER, vbo[2]); //Bind 3rd VBO as active uffer object
	//Copy the texture data from model to the VBO
	glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2) * texCoords.size(), &texCoords[0], GL_STATIC_DRAW);
	//glBufferData(GL_ARRAY_BUFFER, numVerts * 3 * sizeof(GLfloat), (GLfloat*)verts, GL_STATIC_DRAW);
	// texture data is going into attribute index 2 & has 2 floats per vertex
	glVertexAttribPointer(ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_TEXTURE0);  //Enable attribute index 0 (normals)
}

void SkyBox::Render(MatrixStack &projectionStack, MatrixStack &modelviewStack, glm::vec3 position, bool wireFrame)
{
	glCullFace(GL_FRONT);
	//glBindVertexArray(vao); //Bind vertex array.
	glUseProgram(shader);
			
	glm::mat4 skyBoxMatrix = modelviewStack.getMatrix();
	skyBoxMatrix = glm::translate(skyBoxMatrix, position);
	skyBoxMatrix = glm::scale(skyBoxMatrix, glm::vec3(1000.0f, 1000.0f, 1000.0f));
	
	int uniformIndex = glGetUniformLocation(shader, "modelview");
	glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, glm::value_ptr(skyBoxMatrix));
	uniformIndex = glGetUniformLocation(shader, "projection");
	glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, glm::value_ptr(projectionStack.getMatrix()));
	uniformIndex = glGetUniformLocation(shader, "cubeMap");
	glUniform1i(uniformIndex, 0);

	//with no vao in WebGL we must set up the buffers ourselves.
	//data not is loaded in using the method SetUpVAO[sic]
	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);  //Enable attribute index 0 (position)
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); //Bind 1st VBO as active uffer object
	glVertexAttribPointer(ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glEnableVertexAttribArray(ATTRIBUTE_NORMAL);  //Enable attribute index 0 (normals)
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); //Bind 2nd VBO as active uffer object
	glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);

	glEnableVertexAttribArray(ATTRIBUTE_TEXTURE0);  //Enable attribute index 0 (normals)
	glBindBuffer(GL_ARRAY_BUFFER, vbo[2]); //Bind 3rd VBO as active uffer object
	glVertexAttribPointer(ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	

	if (wireFrame)
	{
		glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
		glDrawArrays(GL_LINES, 0, vertices.size());
	}
	else
	{
		glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
		glDrawArrays(GL_TRIANGLES, 0, vertices.size());
	}
	glDisableVertexAttribArray(ATTRIBUTE_VERTEX);
	glDisableVertexAttribArray(ATTRIBUTE_NORMAL);
	glDisableVertexAttribArray(ATTRIBUTE_TEXTURE0);
	//glDisable(GL_CULL_FACE);
}