#include "SoundBank.h"

SoundBank* SoundBank::soundBank = 0;

SoundBank::SoundBank()
{
}

void SoundBank::InitSounds(void)
{
	int ok = Mix_OpenAudio(0, 0, 0, 0);
	if(ok != 0) {
		std::cout << "Trouble setting up sound bank" << ok << std::endl;
	}

	navigate.loadSound("Res/navigate.wav");
	confirm.loadSound("Res/confirm.wav");
	rolling.loadSound("Res/rolling.wav");
	rock.loadSound("Res/RetroFutureNasty.ogg");
	electro.loadSound("Res/BrokenReality.ogg");
	weird.loadSound("Res/JauntyGumption.ogg");
}

void SoundBank::PauseMusic() {
	if(Mix_PausedMusic() == 0) {
		Mix_PauseMusic();
	}
}

void SoundBank::ResumeMusic() {
	if(Mix_PausedMusic() == 1) {
		Mix_ResumeMusic();
	}
}

void SoundBank::StopAllSounds(void)
{
	Mix_HaltMusic();
}

SoundBank::~SoundBank()
{
	Mix_CloseAudio();
}