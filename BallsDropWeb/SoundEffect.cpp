#include "SoundEffect.h"

void SoundEffect::loadSound(char *filename){
	Sound = Mix_LoadWAV(filename);
	if(!Sound) {
		std::cout << "Troubled set up sound file : " << filename << std::endl;
	} else {
		std::cout << "Sound file opened " << filename << std::endl;
	}
	//Buffer.loadFromFile(filename);
	//Sound.setBuffer(Buffer);
}

void SoundEffect::play(){
	//Sound.play();
	channel = Mix_PlayChannel(-1, Sound, 0);
}

void SoundEffect::stop()
{
	Mix_HaltChannel(channel);
	//Sound.stop();
}

void SoundEffect::SetLoop(bool loop){
	
	looped = loop;
}