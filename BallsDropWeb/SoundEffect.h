#include <SDL\SDL_audio.h>
#include <SDL/SDL_mixer.h>
#include <iostream>

#ifdef _MSC_VER
	#pragma once
#endif

#ifndef SOUNDEFFECT_H
#define SOUNDEFFECT_H

class SoundEffect{
public:
	void loadSound(char *filename);
	void play();
	void stop();
	int GetStatus(void) { return Mix_Playing(channel); }
	//sf::Sound::Status GetStatus(void) { return Sound.getStatus(); }
	void SetLoop(bool loop);

private:
	//sf::SoundBuffer Buffer;
	//sf::Sound Sound;
	Mix_Chunk *Sound;
	int channel; //last channel this was associated with
	bool looped; //shoudl this sound be looped?
};


#endif