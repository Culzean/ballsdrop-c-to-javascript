#ifndef STATE_H
#define STATE_H

#include <SDL/SDL.h>
//#include "CXBOXController.h"
#include "WindowManager.h"
#include "ShaderManager.h"
#include "SoundBank.h"

GLfloat const TIME_SECOND = 1000;
GLfloat const TIME_REPEAT_KEY = 165.0f;

class State{

public:
	virtual ~State(){};
	virtual void Init(WindowManager windowManager, ShaderManager shaderManager, int musicSelection) = 0;
	virtual void Update(GLfloat dt, float fps = 0) = 0;
	//virtual void GetInput(CXBOXController *Player1) = 0;
	virtual void KeyHandler( SDL_Event sdlEvent )  = 0;
	virtual void Render(bool WIRE_FRAME) = 0;
	virtual void Cleanup(void) = 0;
	virtual int	 GetSelection(void) = 0;
	virtual void SetSelection(int value) = 0;
	virtual int GetMusic(void) = 0;
	virtual float GetTime(void) = 0;
	virtual void AddTime(float time) = 0;

	bool getPaused(){return UPDATING;};
	bool getQuit(){return EXIT;};
protected:

	bool EXIT;
	bool UPDATING;
	bool repeat;
	float repeatTimer;
	float starter;
};

#endif