#include "Terrain.h"

Terrain::Terrain(void)
{
	//load this file in first thing, make sure it is there when we want it
	height = new HeightField("Res/heightmap.txt");
}

Terrain::~Terrain(void)
{
	CleanUp();
}

void Terrain::CleanUp() {

	glDeleteBuffers(4, vbo);

	delete[] vertices;
	delete[] texCoords;
	delete[] normals;

	delete[] tempTexCoords;
	delete[] tempVerts;
	delete[] tempNormals;
	delete[] indices;
	delete height;

}

void Terrain::ApplyTexture(GLuint texture)
{
	std::cout << "Terrain texture on load " << texture << std::endl;
	this->texture = texture;
}

void Terrain::ApplyShader(GLuint shader)
{
	this->shader = shader;
}

void Terrain::genVerts(int _sizeX, int _sizeZ)
{
	
	tempVerts = new glm::vec3[numVerts];

	float stepX = (float)_sizeX / numCellsWide;
	float stepZ = (float)_sizeZ / numCellsHigh;

	//set start position
	glm::vec3 pos =  glm::vec3(0.0f, 0.0f, 0.0f);

	int count = 0;
	//loop across and up
	for(int z = 0; z < numVertsZ; z++)
	{
		pos.x = 0.0f;

		for(int x = 0; x < numVertsX; x++)
		{
			//Create the verts
			tempVerts[count].x = pos.x;
			tempVerts[count].y = height->getHeight(x,z) / 50;
			tempVerts[count].z = pos.z;
			//increment x across
			pos.x += stepX;

			count++;

		}
		//increment Z
		pos.z+=stepZ;
	}
}

void Terrain::createIndexList(void)
{
	GLuint count = 0;
	GLuint vIndex = 0;

	indices = new GLuint[numVerts * 6];

	for(int z = 0; z < numCellsHigh; z++)
	{
		for(int x = 0; x < numCellsWide; x++)
		{
			//first traingle
			indices[count++] = vIndex;
			indices[count++] = vIndex + numVertsX;
			indices[count++] = vIndex + numVertsX + 1;


			//second triangle
			indices[count++] = vIndex;
			indices[count++] = vIndex + 1;
			indices[count++] = vIndex + numVertsX + 1;

			vIndex++;	
		}

		vIndex++;
	}

	indexSize = count;

}

void Terrain::calcNormals(void)
{
	tempNormals = new glm::vec3[numVerts];
	float zSize = (GLfloat)numVertsZ;
	float xSize = (GLfloat)numVertsX;
	int count = 0;
	glm::vec3 v1, v2, v3, v4, v5, v6 , n1, n2, n3, n4, n5, n6, tempNorm;

	for(int z = 0; z < numVertsZ-1; z++)
	{

		for(int x = 0; x < numVertsX-1; x++)
		{
			if(z == 0)
			{
				if(x == 0)
				{
					v1 = glm::normalize(tempVerts[numVertsX] - tempVerts[0]);
					v2 = glm::normalize(tempVerts[1] - tempVerts[0]);

					tempNormals[count] = v1 * v2;
				}
				else
				if(x == (numVertsX-1))
				{
					v1 = glm::normalize(tempVerts[count - 1] - tempVerts[count]);
					v2 = glm::normalize(tempVerts[count + (numVertsX-1)] - tempVerts[count]);

					n1 = v1 * v2;

					v3 = glm::normalize(tempVerts[count + numVertsX] - tempVerts[count]);

					n2 = v2 * v3;

					tempNorm.x = (n1.x + n2.x)/2;
					tempNorm.y = (n1.y + n2.y)/2;
					tempNorm.z = (n1.z + n2.z)/2;
					tempNormals[count] = tempNorm;

				}
				else
				{
					v1 = glm::normalize(tempVerts[count-1] - tempVerts[count]);
					v2 = glm::normalize(tempVerts[count+(numVertsX-1)] - tempVerts[count]);

					n1 = v1 * v2;

					v3 = glm::normalize(tempVerts[count + numVertsX] - tempVerts[count]);

					n2 = v2 * v3;

					v4 = glm::normalize(tempVerts[count+1] - tempVerts[count]);

					n3 = v3 * v4;

					tempNorm.x = (n1.x + n2.x + n3.x)/3;
					tempNorm.y = (n1.y + n2.y + n3.y)/3;
					tempNorm.z = (n1.z + n2.z + n3.z)/3;
					tempNormals[count] = tempNorm;

				}
			}
			else
			if(x == 0)
			{
				if(z == (numVertsZ -1))
				{
					v1 = glm::normalize(tempVerts[count - numVertsX] - tempVerts[count]);
					v2 = glm::normalize(tempVerts[count - (numVertsX - 1)] - tempVerts[count]);

					n1 = v1 * v2;

					v3 = glm::normalize(tempVerts[count + 1] - tempVerts[count]);

					n2 = v2 * v3;

					tempNorm.x = (n1.x + n2.x)/2;
					tempNorm.y = (n1.y + n2.y)/2;
					tempNorm.z = (n1.z + n2.z)/2;
					tempNormals[count] = tempNorm;
				}
				else
				{
					v1 = glm::normalize(tempVerts[count - numVertsX] - tempVerts[count]);
					v2 = glm::normalize(tempVerts[count - (numVertsX-1)] - tempVerts[count]);

					n1 = v1 * v2;

					v3 = glm::normalize(tempVerts[count + 1] - tempVerts[count]);

					n2 = v2 * v3;

					v4 = glm::normalize(tempVerts[count + numVertsX] - tempVerts[count]);

					n3 = v3 * v3;

					tempNorm.x = (n1.x + n2.x + n3.x)/3;
					tempNorm.y = (n1.y + n2.y + n3.y)/3;
					tempNorm.z = (n1.z + n2.z + n3.z)/3;
					tempNormals[count] = tempNorm;

				}
			}
			else
			if(x == numVertsX-1)
			{
				if(z == numVertsZ-1)
				{
					v1 = glm::normalize(tempVerts[count - 1] - tempVerts[count]);
					v2 = glm::normalize(tempVerts[count - numVertsX] - tempVerts[count]);

					tempNormals[count] = v1 * v2;

				}
				else
				{
					v1 = glm::normalize(tempVerts[count - numVertsX] - tempVerts[count]);
					v2 = glm::normalize(tempVerts[count - 1] - tempVerts[count]);

					n1 = v1 * v2;

					v3 = glm::normalize(tempVerts[count + (numVertsX-1)] - tempVerts[count]);

					n2 = v2 * v3;

					v4 = glm::normalize(tempVerts[count + numVertsX] - tempVerts[count]);

					n3 = v3 * v4;

					tempNorm.x = (n1.x + n2.x + n3.x)/3;
					tempNorm.y = (n1.y + n2.y + n3.y)/3;
					tempNorm.z = (n1.z + n2.z + n3.z)/3;
					tempNormals[count] = tempNorm;

				}
			}
			else
			if(z == numVertsZ - 1)
			{
				v1 = glm::normalize(tempVerts[count - 1] - tempVerts[count]);
				v2 = glm::normalize(tempVerts[count - numVertsX] - tempVerts[count]);

				n1 = v1 * v2;

				v3 = glm::normalize(tempVerts[count - (numVertsX-1)] - tempVerts[count]);

				n2 = v2 * v3;

				v4 = glm::normalize(tempVerts[count + 1] - tempVerts[count]);

				n3 = v3 * v4;

				tempNorm.x = (n1.x + n2.x + n3.x)/3;
				tempNorm.y = (n1.y + n2.y + n3.y)/3;
				tempNorm.z = (n1.z + n2.z + n3.z)/3;
				tempNormals[count] = tempNorm;

			}
			else
			{
				v1 = glm::normalize(tempVerts[count - numVertsX] - tempVerts[count]);
				v2 = glm::normalize(tempVerts[count - (numVertsX-1)] - tempVerts[count]);

				n1 = v1 * v2;

				v3 = glm::normalize(tempVerts[count + 1] - tempVerts[count]);

				n2 = v2 * v3;

				v4 = glm::normalize(tempVerts[count + numVertsX] - tempVerts[count]);

				n3 = v3 * v4;

				v5 = glm::normalize(tempVerts[count + (numVertsX-1)] - tempVerts[count]);

				n4 = v4 * v5;

				v6 = glm::normalize(tempVerts[count - 1] - tempVerts[count]);

				n5 = v5 * v6;

				n6 = v6 * v1;

				tempNorm.x = (n1.x + n2.x + n3.x + n4.x + n5.x + n6.x)/6;
				tempNorm.y = (n1.y + n2.y + n3.y + n4.y + n5.y + n6.y)/6;
				tempNorm.z = (n1.z + n2.z + n3.z + n4.z + n5.z + n6.z)/6;
				tempNormals[count] = tempNorm;

			}


			count++;
		}
	}

}

void Terrain::calcTexCoords(void)
{
	tempTexCoords = new glm::vec3[numVerts];
	int count = 0;

	for(int z = 0; z < numVertsZ; z++)
	{
		for(int x = 0; x < numVertsX; x++)
		{			
			tempTexCoords[count].x = (float)x / numVertsX;
			tempTexCoords[count].y = (float)z / numVertsZ;

			count++;
		}
	}

}

void Terrain::indexVerts(void)
{
	/*int indexValue = 0;
	vertices = new glm::vec3[indexSize];
	normals = new glm::vec3[indexSize];
	texCoords = new glm::vec2[indexSize];

	for(int i = 0; i < indexSize; i++)
	{
		indexValue = indices[i];

		vertices[i].x = tempVerts[indexValue].x;
		vertices[i].y = tempVerts[indexValue].y;
		vertices[i].z = tempVerts[indexValue].z;

		normals[i].x = tempNormals[indexValue].x;
		normals[i].y = tempNormals[indexValue].y;
		normals[i].z = tempNormals[indexValue].z;

		texCoords[i].x = tempTexCoords[indexValue].x;
		texCoords[i].y = tempTexCoords[indexValue].y;

	}*/

}


void Terrain::alignBuffers() {

	vertices = new glm::vec3[numVerts];
	normals = new glm::vec3[numVerts];
	texCoords = new glm::vec2[numVerts];

	for(int i = 0; i < numVerts; i++)
	{
		vertices[i].x = tempVerts[i].x;
		vertices[i].y = tempVerts[i].y;
		vertices[i].z = tempVerts[i].z;

		normals[i].x = tempNormals[i].x;
		normals[i].y = tempNormals[i].y;
		normals[i].z = tempNormals[i].z;

		texCoords[i].x = tempTexCoords[i].x;
		texCoords[i].y = tempTexCoords[i].y;
	}
}

void Terrain::Generate(int _numCellsX, int _numCellsZ, int _sizeX, int _sizeZ)
{
	numCellsWide = _numCellsX;
	numCellsHigh = _numCellsZ;

	numVertsX = numCellsWide + 1;
	numVertsZ = numCellsHigh + 1;

	numVerts = numVertsX * numVertsZ;


	//////////////////////////////////
	//Light Settings/////////////////
	////////////////////////////////
	light0.ambient = glm::vec4( 0.7, 0.7, 0.7, 1.0 );
	light0.diffuse = glm::vec4(0.7, 0.7, 0.7, 1.0 );
	light0.specular = glm::vec4(0.7, 0.7, 0.7, 1.0 );

	lightPosition = glm::vec4(-1.0, 5.0, -4.0, 1.0 );


	/////////////////////////////////
	//Material setting//////////////
	///////////////////////////////
	material0.ambient = glm::vec4(0.5, 0.5, 0.5, 1.0);
	material0.diffuse = glm::vec4(1.0, 1.0, 1.0, 1.0);
	material0.specular = glm::vec4(0.5, 0.5, 0.5, 1.0);
	material0.shininess = 1.0;

	height->Create(numVertsX, numVertsZ);

	/////////////////////////////////
	/////GENERATE VERTICES//////////
	///////////////////////////////
	genVerts(_sizeX, _sizeZ);

	///////////////////////////////////////////
	/////CREATE INDEX LIST////////////////////
	/////////////////////////////////////////
	createIndexList();


	///////////////////////////////////////////
	//Gernerate Normals///////////////////////
	/////////////////////////////////////////
	calcNormals();
	

	////////////////////////////////////////////
	////Calculate Texture Coordinates//////////
	//////////////////////////////////////////
	calcTexCoords();


	//////////////////////////////////////////
	//INDEX THE VERTICES/////////////////////
	////////////////////////////////////////
	glGenBuffers(4, vbo); // Allocate three Vertex Buffer Object (VBO)
	alignBuffers(); //this call copies the min. no. of verts and requires a element buffer for indexing
	//indexVerts(); //this lines up all the verts in order and is not efficient
	

	initBuffers();
}

void Terrain::initBuffers(void)
{
	GLboolean trueBuff = false;
	//glGenVertexArrays(1, &vao); // Allocate & assign Vertex Array Object (VAO)
	//glBindVertexArray(vao); // Bind VAO as current object
	 
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); //Bind 1st VBO as active uffer object
	//Copy the vertex data from model to VBO
	glBufferData(GL_ARRAY_BUFFER, numVerts * sizeof(glm::vec3), vertices, GL_DYNAMIC_DRAW);
	// Position data is going into attribute index 0 & has 3 floats per vertex
	glVertexAttribPointer(ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);  //Enable attribute index 0 (position)

	if(glIsBuffer(vbo[0]))
		std::cout << "buffer is valid : " << vbo[0]  << std::endl;
	else
		std::cout << "INVALID buffer : " << vbo[0]  << std::endl;

	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); //Bind 2nd VBO as active uffer object
	//Copy the normal data from Model to the VBO
	glBufferData(GL_ARRAY_BUFFER, numVerts * sizeof(glm::vec3), normals, GL_DYNAMIC_DRAW);
	// Normal data is going into attribute index 1 & has 3 floats per vertex
	glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_NORMAL);  //Enable attribute index 1 (Normals)

	if(glIsBuffer(vbo[1]))
		std::cout << "buffer is valid : " << vbo[1]  << std::endl;
	else
		std::cout << "INVALID buffer : " << vbo[1]  << std::endl;

	glBindBuffer(GL_ARRAY_BUFFER, vbo[2]); //Bind 3rd VBO as active uffer object
	//Copy the texture coord data from Model to the VBO
	glBufferData(GL_ARRAY_BUFFER, numVerts * sizeof(glm::vec2), texCoords, GL_DYNAMIC_DRAW);
	// Normal data is going into attribute index 1 & has 2 floats per vertex
	glVertexAttribPointer(ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_TEXTURE0);  //Enable attribute index 2 (texCoords)

	if(glIsBuffer(vbo[2]))
		std::cout << "buffer is valid : " << vbo[2]  << std::endl;
	else
		std::cout << "INVALID buffer : " << vbo[2]  << std::endl;

	//bind the index list
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[3]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indexSize * sizeof(GLuint), indices, GL_DYNAMIC_DRAW);

	if(glIsBuffer(vbo[3]))
		std::cout << "buffer is valid : " << vbo[3]  << std::endl;
	else
		std::cout << "INVALID buffer : " << vbo[3]  << std::endl;

	//glBindVertexArray(0);
}

void Terrain::Update(glm::vec3 position)
{
	this->terrainX = (GLint)position.x  - 1125.0f;
	this->terrainY = (GLint)position.y  - 3500.0f;
	this->terrainZ = (GLint)position.z  - 1700.0f;
}

void Terrain::Render(MatrixStack &projectionStack, MatrixStack &modelviewStack)
{
	//glDisable(GL_CULL_FACE);
	//glBindVertexArray(vao); // Bind VAO as current object
	glUseProgram(shader);
	//initBuffers(); //use this method to do all the vbo binding

	glm::mat4 terrainModelView = modelviewStack.getMatrix();


	terrainModelView = glm::translate(terrainModelView, glm::vec3(terrainX, terrainY, terrainZ));

	terrainModelView = glm::scale(terrainModelView, glm::vec3(750.0f,950.0f,750.0f));

	int uniformIndex = glGetUniformLocation(shader, "projection");
	glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(projectionStack.getMatrix()));
	uniformIndex = glGetUniformLocation(shader, "modelview");
	glUniformMatrix4fv(uniformIndex, 1,GL_FALSE,glm::value_ptr(terrainModelView));
	

	// light0 is C++ struct similar to GLSL struct shown
	uniformIndex = glGetUniformLocation(shader, "light.ambient");
	glUniform4fv(uniformIndex, 1, glm::value_ptr(light0.ambient ));
	
	uniformIndex = glGetUniformLocation(shader, "light.diffuse");
	glUniform4fv(uniformIndex, 1, glm::value_ptr(light0.diffuse));
	
	uniformIndex = glGetUniformLocation(shader, "light.specular");
	glUniform4fv(uniformIndex, 1, glm::value_ptr(light0.specular));

	lightPosition = glm::vec4(-1.0, 1.0, -4.0, 1.0 );


	uniformIndex = glGetUniformLocation(shader, "lightPosition");
	glUniform4fv(uniformIndex, 1, glm::value_ptr(lightPosition));


	// material0 is C++ struct similar to GLSL struct shown
	uniformIndex = glGetUniformLocation(shader, "material.ambient");
	glUniform4fv(uniformIndex, 1, glm::value_ptr(material0.ambient));
	
	uniformIndex = glGetUniformLocation(shader, "material.diffuse");
	glUniform4fv(uniformIndex, 1, glm::value_ptr(material0.diffuse));
	
	uniformIndex = glGetUniformLocation(shader, "material.specular");
	glUniform4fv(uniformIndex, 1, glm::value_ptr(material0.specular));
	
	uniformIndex = glGetUniformLocation(shader, "material.shininess");
	glUniform1f(uniformIndex, material0.shininess);
	
	glDisableVertexAttribArray(1);
	glDisableVertexAttribArray(3);

	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);  //Enable attribute index 0 (position)
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]); //Bind 1st VBO as active uffer object
	// Position data is going into attribute index 0 & has 3 floats per vertex
	glVertexAttribPointer(ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	
	glEnableVertexAttribArray(ATTRIBUTE_NORMAL);  //Enable attribute index 1 (Normals)
	glBindBuffer(GL_ARRAY_BUFFER, vbo[1]); //Bind 2nd VBO as active uffer object
	// Normal data is going into attribute index 1 & has 3 floats per vertex
	glVertexAttribPointer(ATTRIBUTE_NORMAL, 3, GL_FLOAT, GL_FALSE, 0, 0);
	
	glEnableVertexAttribArray(ATTRIBUTE_TEXTURE0);  //Enable attribute index 2 (texCoords)
	glBindBuffer(GL_ARRAY_BUFFER, vbo[2]); //Bind 3rd VBO as active uffer object
	// Normal data is going into attribute index 1 & has 2 floats per vertex
	glVertexAttribPointer(ATTRIBUTE_TEXTURE0, 2, GL_FLOAT, GL_FALSE, 0, 0);
	
	//glActiveTexture(GL_TEXTURE0);
	//glBindTexture(GL_TEXTURE_2D, texture);
	//std::cout << "Terrain guint BROKE" << texture << std::endl;

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vbo[3]);

	glDrawElements(GL_TRIANGLES, indexSize, GL_UNSIGNED_INT, 0 );
	//glDrawArrays(GL_TRIANGLES, 0, indexSize);

	glDisableVertexAttribArray(ATTRIBUTE_VERTEX);
	glDisableVertexAttribArray(ATTRIBUTE_NORMAL);
	glDisableVertexAttribArray(ATTRIBUTE_TEXTURE0);
	//glUseProgram(0);
	//glBindVertexArray(0);

	//glEnable(GL_CULL_FACE);
}