#include "TextArea.h"

///////////////////////////////////////////////////////////////////////////////////////
//The code for placing text to a texture is taken from Daniel Livingstones Label class.
///////////////////////////////////////////////////////////////////////////////////////


TextArea::TextArea(glm::vec3 centerPosition, char *fontFile, char *message, SDL_Color color, ShaderManager shaderManager)
{
	Error::CheckTTFInit();
	font = TTF_OpenFont(fontFile, 64);
	Error::CheckTTFOpenFont(font);
	textColor = color;
	centerPos = centerPosition;
	this->message = message;
	alpha = 1.0;
	shaderP = shaderManager.TEXTURE_ONLY;
	glGenTextures(1, &tex);
	TextToTextureBestFit(tex, message, font, textColor, width, height);
	MakeQuad(centerPosition, width, height);
}

TextArea::TextArea(glm::vec3 centerPosition, char *fileName, ShaderManager shaderManager)
{
	centerPos = centerPosition;
	shaderP = shaderManager.TEXTURE_ONLY;
	tex = TextureManager::loadTexture(fileName);
	alpha = 1.0;
	MakeQuad(centerPosition, width, height);
}

/**
 * \brief Finds the next power of two for an integer.
 * \param v The integer to find the next power of two for.
 * \return The next power of two of v.
 *
 * This code is directly copy/pasted from:
 * http://graphics.stanford.edu/~seander/bithacks.html#RoundUpPowerOf2
 */
static unsigned int NextPowerOfTwo( unsigned int v ) {
  v--;
  v |= v >> 1;
  v |= v >> 2;
  v |= v >> 4;
  v |= v >> 8;
  v |= v >> 16;
  v++;
  return v;
}

/**
 * \brief Moves a texture from one memory block to the other.
 * \param src Source surface.
 * \param dest Destination surface.
 *
 * Moves the contents of src to dest, provided that dest is equal or greater in
 * size. It will put it in the upper-left-hand corner (0,0).  Make sure both
 * surfaces are 32-bit with alpha channels.
 copied from https://github.com/gpcz/OpenGL-SDL-Code-Warehouse/blob/master/SDLGLTexture.cpp
 */
static void MoveTexture( SDL_Surface *src, SDL_Surface *dest ) {
  Sint32 x, y;
  Uint32 *srcPixels, *destPixels;

  if ( src && dest ) {
    if ( dest->w >= src->w && dest->h >= src->h ) {
      // You need to lock surfaces before handling their raw pixels.
      SDL_LockSurface( dest );
      SDL_LockSurface( src );
      for ( y = 0; y < src->h; y++ ) {
        // The source's pixels are easy: a row
        // start is pixels+y*src->w.
        srcPixels = (Uint32*)src->pixels + y*src->w;
        // Destination's pixel rowstarts are dest->pixels + y*dest->w.
        destPixels = (Uint32*)dest->pixels + y*dest->w;
        for ( x = 0; x < src->w; x++ ) {
          *destPixels = *srcPixels;
          destPixels++;
          srcPixels++;
        }
      }
      // We've done what we need to do.  Time to clean up.
      SDL_UnlockSurface( src );
      SDL_UnlockSurface( dest );
    }
  }
}

/**
searches for a the next power of two for the texture's w and h
rebuilds the texture into a new surface using the new dimensions
returns the surface
bassed on https://github.com/gpcz/OpenGL-SDL-Code-Warehouse/blob/master/SDLGLTexture.cpp
 */

void TextArea::TextToTextureBestFit(GLuint &texture, const char *message, TTF_Font *font, SDL_Color colour, GLuint &width, GLuint &height) {
        SDL_Surface *stringImage;
		SDL_Surface* tempImage;
		Uint32 rmask, gmask, bmask, amask;

		tempImage = TTF_RenderText_Blended(font, message, colour);
		SDL_Color bg = {255, 0, 0};

        Error::TTF_RenderText_Blended(tempImage);

        

		#if SDL_BYTEORDER == SDL_BIG_ENDIAN
		  rmask = 0xff000000;
		  gmask = 0x00ff0000;
		  bmask = 0x0000ff00;
		  amask = 0x000000ff;
		#else
		  rmask = 0x000000ff;
		  gmask = 0x0000ff00;
		  bmask = 0x00ff0000;
		  amask = 0xff000000;
		#endif

		stringImage = SDL_CreateRGBSurface( SDL_SWSURFACE | SDL_SRCALPHA, NextPowerOfTwo(tempImage->w), NextPowerOfTwo(tempImage->h), 32, rmask, gmask, bmask, amask );
		//SDL_FillRect( stringImage, &stringImage->clip_rect,  SDL_MapRGBA( stringImage->format, bg.r,bg.r,bg.r,bg.r )  );
		SDL_Rect tempTect0 = stringImage->clip_rect;
		SDL_Rect tempTect1 = tempImage->clip_rect;
		SDL_BlitSurface (tempImage, NULL, stringImage, NULL);
		//SDL_BlitSurface(tempImage, &tempImage->clip_rect, stringImage, &stringImage->clip_rect);
		//convertedImage = SDL_ConvertSurface( tempImage, stringImage->format, SDL_SWSURFACE | SDL_SRCALPHA );

		//MoveTexture( convertedImage, stringImage );

		width = stringImage->w;
        height = stringImage->h;

        GLuint colours = stringImage->format->BytesPerPixel;

        GLuint format, internalFormat;
        if (colours == 4) {   // alpha
                if (stringImage->format->Rmask == 0x000000ff)
                        format = GL_RGBA;
            else
                    format = GL_BGRA;
        } else {             // no alpha
                if (stringImage->format->Rmask == 0x000000ff)
                        format = GL_RGB;
            else
                    format = GL_BGR;
        }
        internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

		glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);

		glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);

        // SDL surface was used to generate the texture but is no longer
        // required. Release it to free memory
        SDL_FreeSurface(stringImage);
		SDL_FreeSurface(tempImage);
}

void TextArea::TextToTexture(GLuint &texture, const char *message, TTF_Font *font, SDL_Color colour, GLuint &width, GLuint &height) {
        TextToTextureBestFit(texture, message, font, colour, width, height);
		SDL_Surface *stringImage;
		stringImage = TTF_RenderText_Blended(font, message, colour);
		SDL_Color bg = {0, 0, 0};

        Error::TTF_RenderText_Blended(stringImage);

        width = stringImage->w;
        height = stringImage->h;

        GLuint colours = stringImage->format->BytesPerPixel;

        GLuint format, internalFormat;
        if (colours == 4) {   // alpha
                if (stringImage->format->Rmask == 0x000000ff)
                        format = GL_RGBA;
            else
                    format = GL_BGRA;
        } else {             // no alpha
                if (stringImage->format->Rmask == 0x000000ff)
                        format = GL_RGB;
            else
                    format = GL_BGR;
        }
        internalFormat = (colours == 4) ? GL_RGBA : GL_RGB;

		glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, texture);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE );
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE );
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER,GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER,GL_LINEAR);

		glTexImage2D(GL_TEXTURE_2D, 0, internalFormat, width, height, 0,
                    format, GL_UNSIGNED_BYTE, stringImage->pixels);

        // SDL surface was used to generate the texture but is no longer
        // required. Release it to free memory
        SDL_FreeSurface(stringImage);
}

void TextArea::UpdateTextString(char *newMessage)
{
	TextToTextureBestFit(tex, newMessage, font, textColor, width, height);
}

void TextArea::UpdateTextInteger(int value)
{
	ss << value;
	TextToTextureBestFit(tex, ss.str().c_str() , font, textColor , width, height);
	//TextToTexture(tex, ss.str().c_str() , font, textColor , width, height);	
	ss.str(std::string());
	//TextToTexture(tex, string, font, textColor, width, height);
}

void TextArea::UpdateTextFloat(float value)
{
	ss << value;
	//sprintf_s(string, "%.2f", value);
	//TextToTexture(tex, ss.str().c_str(), font, textColor, width, height);
	TextToTextureBestFit(tex, ss.str().c_str(), font, textColor, width, height);	
	ss.str(std::string());
	//ss.clear(); does not do what you think it does. Clears errors state
}

void TextArea::SetColour(SDL_Color newColor)
{
	textColor = newColor;
	TextToTextureBestFit(tex, this->message, font, textColor, width, height);
} 

void TextArea::MakeQuad(glm::vec3 centerPosition, GLuint width, GLuint height)
{
	verts[0] = glm::vec3(0.0, 0.0, 10.0);
	verts[1] = glm::vec3(1.0, 0.0, 10.0);
	verts[2] = glm::vec3(1.0, 1.0, 10.0);
	verts[3] = glm::vec3(0.0, 1.0, 10.0);

	//glGenVertexArrays(1, &vao); // Allocate & assign Vertex Array Object (VAO)
	//glBindVertexArray(vao); // Bind VAO as current object

	glGenBuffers(1, vbo); // Allocate three Vertex Buffer Object (VBO)
	
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, 4 * sizeof(glm::vec3), verts, GL_STATIC_DRAW);
	glVertexAttribPointer(ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);
}

void TextArea::Render(MatrixStack &projectionStack, MatrixStack &modelviewStack, bool WIRE_FRAME, bool FLIPPED)
{
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//included here instead of a vao object
	glEnableVertexAttribArray(ATTRIBUTE_VERTEX);
	glBindBuffer(GL_ARRAY_BUFFER, vbo[0]);
	glVertexAttribPointer(ATTRIBUTE_VERTEX, 3, GL_FLOAT, GL_FALSE, 0, 0);
	

	glm::mat4 matrix(1.0);
	modelviewStack.push(matrix);
	
	matrix = glm::translate(matrix, centerPos);
	matrix = glm::rotate(matrix, 180.0f, glm::vec3(0.0f, 1.0f, 0.0f));

	/*matrix= glm::scale( glm::vec3(1.0f, 4.8f, 1.0f) );
	matrix = glm::rotate(matrix, 180.0f, glm::vec3(0.0f, 1.0f, 0.0f));
	matrix = glm::translate(matrix, centerPos);*/

	modelviewStack.multiplyMatrix(matrix);

		//glBindVertexArray(vao);
		glUseProgram(shaderP);
		int	uniformIndex = glGetUniformLocation(shaderP, "projection");
		glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, glm::value_ptr(projectionStack.getMatrix()));
		uniformIndex = glGetUniformLocation(shaderP, "modelview");
		glUniformMatrix4fv(uniformIndex, 1, GL_FALSE, glm::value_ptr(modelviewStack.getMatrix()));
		uniformIndex = glGetUniformLocation(shaderP, "textureUnit0");
		glUniform1i(uniformIndex, 0);
		uniformIndex = glGetUniformLocation(shaderP, "flipped");
		glUniform1i(uniformIndex, FLIPPED);
		uniformIndex = glGetUniformLocation(shaderP, "alpha");
		glUniform1f(uniformIndex, alpha);


		if (WIRE_FRAME)
		{
			glDrawArrays(GL_LINE_LOOP, 0, 4);
		}
		else
		{
			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, tex);
			glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
			glBindTexture(GL_TEXTURE_2D, 0);
			glActiveTexture(GL_TEXTURE0);
		}

	modelviewStack.pop();
}

TextArea::~TextArea(void)
{
	glDeleteBuffers(1,vbo);
}