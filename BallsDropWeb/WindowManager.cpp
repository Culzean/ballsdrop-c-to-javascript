#include "WindowManager.h"
#include <emscripten.h>

SDL_Surface* WindowManager::SetupWebRC( int width, int height ) {

	SDL_Surface *screen;
	WIDTH = width;
	HEIGHT = height;
	emscripten_set_canvas_size(WIDTH, HEIGHT);

    // Slightly different SDL initialization
	if (SDL_Init(SDL_INIT_AUDIO )) //Initialize video
		printf("Unable to initialize Audio: %s\n", SDL_GetError());
	else
		std::cout << "SDL Aduio initialized without fault." << std::endl;
    if (SDL_Init(SDL_INIT_VIDEO )) //Initialize video
		printf("Unable to initialize SDL: %s\n", SDL_GetError());
	else
		std::cout << "SDL initialized without fault." << std::endl;

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);	//double buffering on!
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

	//Turn on x4 multisampling anti-aliasing (MSAA)
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 4);


    screen = SDL_SetVideoMode( width, height, 32, SDL_OPENGL ); // *changed*
    if ( !screen ) {
        printf("Unable to set video mode: %s\n", SDL_GetError());
        return NULL;
    }

	return screen;
}


SDL_Window* WindowManager::SetupRC(SDL_GLContext &context, int width, int height, char *windowName)
{
	/*WIDTH = width;
	HEIGHT = height;

	if (SDL_Init(SDL_INIT_VIDEO | (SDL_INIT_AUDIO < 0) )) //Initialize video
		Error::ExitFatalError("Unable to initialize SDL");
	else
		std::cout << "SDL initialized without fault." << std::endl;

	//Request an openGL 3.0 context
	//If you request a context not supported by your drivers,
	//no openGL context will be created

	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
	SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);

	SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1);	//double buffering on!
	SDL_GL_SetAttribute(SDL_GL_ALPHA_SIZE, 8);

	//Turn on x4 multisampling anti-aliasing (MSAA)
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 4);

	//Create 800x600 window
	window = SDL_CreateWindow(windowName, 
		SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		width, height, SDL_WINDOW_OPENGL | SDL_WINDOW_SHOWN);

	if (!window)	//Check window was created OK
		Error::ExitFatalError("Unable to create window");
	else
		std::cout << "Window created without fault." << std::endl;

	//Create openGL context and attach to window
	context = SDL_GL_CreateContext(window);*/

	//return the SDL_Window *
	return window;
}

void WindowManager::ToggleFullscreen(void){

	if (!FULLSCREEN){
		SDL_SetWindowFullscreen(window, SDL_TRUE);
		FULLSCREEN = true;
	}
	else{
		SDL_SetWindowFullscreen(window, SDL_FALSE);
		FULLSCREEN = false;
	}
}