#include "heightField.h"

HeightField::HeightField(char *filename)
{
	heightfname = filename;
}

HeightField::~HeightField(void)
{
	delete[] this->hHeightField;
}

Uint32 HeightField::getpixel(SDL_Surface *surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    /* Here p is the address to the pixel we want to retrieve */
    Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;

    switch(bpp) {
    case 1:
        return *p;
        break;

    case 2:
        return *(Uint16 *)p;
        break;

    case 3:
        if(SDL_BYTEORDER == SDL_BIG_ENDIAN)
            return p[0] << 16 | p[1] << 8 | p[2];
        else
            return p[0] | p[1] << 8 | p[2] << 16;
        break;

    case 4:
        return *(Uint32 *)p;
        break;

    default:
        return 0;       /* shouldn't happen, but avoids warnings */
    }
}

bool HeightField::Create( int width, int height)
{
	std::ifstream heightReader(heightfname);
	//heightReader.open();
	hmHeight = height;
	hmWidth = width;

	std::string line;

	hHeightField = new float*[width];
	for(int i = 0; i < width; i++)
		hHeightField[i] = new float[height];

	if(!heightReader.is_open()) {
		std::cout << "Big trouble opening heightmap file!" << std::endl;
		return false;
	}

	int tempHeight;

	for(int x = 0; x < width; x++)
	{
		for(int z = 0; z < height; z++)
		{
			heightReader >> tempHeight;
			hHeightField[x][z] = (GLfloat)tempHeight;
			//if(x == 24)std::cout << hHeightField[x][z] << "\n";
		}
	}

	heightReader.close();
	return true;
}

void HeightField::Render(void)
{

}