#ifndef _HEIGHTFIELD_H_
#define _HEIGHTFIELD_H_


#include <iostream>
#include <SDL/SDL.h>
#include <SDL\SDL_pixels.h>
#include "stafx.h"

class HeightField
{
	public:
		HeightField(char *filename);
		~HeightField(void);
		bool Create( const int width, const int height);
		void Render(void);

		float getHeight(int _posX, int _posY)  { return hHeightField[_posX][_posY]; };
		int getMapWidth() { return hmWidth; };
		int getMapHeight() { return hmHeight; };

	private:
		char* heightfname;
		Uint32 getpixel(SDL_Surface *surface, int x, int y);
		float** hHeightField;
		int hmHeight;
		int hmWidth;
};

#endif