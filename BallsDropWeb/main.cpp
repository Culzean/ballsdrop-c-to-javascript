#include <SDL/SDL.h>
#include <GL\glew.h>
#include <ctime>
#include <stdio.h>
#include <SDL/SDL.h>
#include <SDL/SDL_ttf.h>
#include <string.h>
#include <emscripten.h>
#include <stdio.h>
#include <stdlib.h>

#include "WindowManager.h"
#include "Demo.h"
#include "Multi.h"
#include "Menu.h"
#include "Options.h"
#include "Finish.h"

#include "stafx.h"
using namespace std;

#if _DEBUG
#pragma comment(linker, "/subsystem:\"console\" /entry:\"WinMainCRTStartup\"")
#endif

bool RUNNING;
bool WIRE_FRAME = false;
int WIDTH = 1280, HEIGHT = 720;	//Window width, height!

SDL_Surface* screen;
WindowManager windowManager;	//WindowManager variable... providing window operations eg. fullscreen. 
ShaderManager shaderManager;
State *demo, *menu, *options, *currentState, *finish, *multi;
/*CXBOXController* Player1, *Player2;
XINPUT_STATE prevState;*/
SoundBank *soundBank;

int musicSelection = 0;
bool keyboardSelect;

#define FRAME_VALUES 10

time_t frameTimes[FRAME_VALUES];
GLint frameCount;

time_t currentTick;
time_t prevTick;
GLint currentFrame;
GLfloat deltaTime;
GLfloat fps;

static bool grabinput = false, minimized = false;
static bool exitBut = false;


void Init(void *arg)
{
	shaderManager.InitShaders();
	soundBank = SoundBank::GetInstance();

	demo = new Demo();
	multi = new Multi(WIDTH, HEIGHT);
	menu = new Menu();
	options = new Options();
	finish = new Finish();

	keyboardSelect = false;
	menu->Init(windowManager, shaderManager, 0);

	/*Player1 = new CXBOXController(1);
	Player2 = new CXBOXController(2);*/
	currentState = menu;

	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_TEXTURE_CUBE_MAP_SEAMLESS); //not valid WebGL
	
	prevTick = 0;
	currentTick = SDL_GetTicks();
	deltaTime = 0;
	frameCount = 0;
	currentFrame = 0;
	memset (frameTimes, 0, sizeof(frameTimes));
	fps = 0;
	soundBank->InitSounds();

	SDL_GL_SetSwapInterval(0);
}


void Cleanup(void)
{
	soundBank->StopAllSounds();
	delete soundBank;
	shaderManager.ClearShaders();
	delete demo;
	delete multi;
	delete menu;
	delete options;
	delete finish;
	//delete(Player1);
	//delete Player2;
}

void quit(void) {

	emscripten_cancel_main_loop();
	glClearColor(1.0f, 1.0f, 1.0f, 1.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	SDL_GL_SwapBuffers();
	Cleanup();
	SDL_FreeSurface(screen);
	//emscripten_exit_with_live_runtime();	
	SDL_Quit();
	exit(EXIT_SUCCESS);
}

bool handleSDLEvent(SDL_Event const &sdlEvent)
{
	//if(sdlEvent.type == SDL_QUIT)
	//	return false;


	currentState->KeyHandler(sdlEvent);

	if(sdlEvent.type == SDL_ACTIVEEVENT) {
		std::cout << "EVENT CLOSED" << "CLOSED!" << std::endl;
		/*if(sdlEvent.active.state & SDL_APPINPUTFOCUS)
            inputgrab(grabinput = sdlEvent.active.gain!=0);*/
        if(sdlEvent.active.state & SDL_APPACTIVE)
            minimized = !sdlEvent.active.gain;
	}

	else if(sdlEvent.type == SDL_VIDEORESIZE)
	{
		std::cout << "Resize event" << std::endl;
	}
	else if(sdlEvent.type == SDL_KEYDOWN)
	{
		//Can extend this to handle a wider range of keys
		switch(sdlEvent.key.keysym.sym)
		{
			case SDLK_0:
			if (WIRE_FRAME)
			{
				std::cout << "RENDER MODE" << std::endl;
				WIRE_FRAME = false;
			}
			else
			{
				std::cout << "WIRE-FRAME MODE" << std::endl;
				WIRE_FRAME = true;
			}
			break;

			case SDLK_RETURN :
				
				keyboardSelect = true;
			break;
			case SDLK_SPACE:
				
				keyboardSelect = true;
			break;

			case SDLK_ESCAPE:
				exitBut = !exitBut;
				return false;
			case SDLK_F1:
				windowManager.ToggleFullscreen();
				break;
			case SDLK_BACKSPACE:
				if(currentState != menu){
					currentState->SetSelection(0);
					currentState->Cleanup();
					//soundBank->StopAllSounds();
					//soundBank->confirm.play();
					menu->Init(windowManager, shaderManager, 0);
					currentState = menu;
				}
				break;
			default:
				break;
		}
	}
	else if(sdlEvent.type == SDL_KEYUP)
	{
		switch(sdlEvent.key.keysym.sym)
		{

			case SDLK_RETURN:
				//keyboardSelect = false;
			break;
			default:
				break;
		}
	}
	else if(sdlEvent.type == SDL_QUIT) {
		if(RUNNING) {
			quit();
			RUNNING = false;
		}
	}
	
	return true;
}

bool UpdateTimeStep() {

	//find the time step and update the average fps
	currentTick = ( SDL_GetTicks() );
	
	deltaTime = (GLfloat)(currentTick - prevTick);

	//std::cout << "Time Stamp " << deltaTime << " Time in milli " << std::endl;
	

	prevTick = currentTick;

	//reset the counter if we have taken a pause. ie debugging
	if(deltaTime > 100) {
		deltaTime = 80;
	}

	//find correct frame we are on
	//and record the time step
	currentFrame = frameCount++ % FRAME_VALUES;
	frameTimes[currentFrame] = (time_t)deltaTime;

	//find fps
	fps = 0;
	for(int i = 0; i < FRAME_VALUES; ++i) {
		fps += frameTimes[i];
	}
	fps /= FRAME_VALUES;
	fps = 1000 / fps;
	return true;
}

void UpdateController(void)
{
	//currentState->GetInput(Player1);

	//if(currentState == multi)
		//currentState->GetInput(Player2);

	//if(Player1->IsConnected())
	{
		

		/*else
		if (currentState == options)
		{
			musicSelection = options->GetMusic();
			if (Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_B)
			{
				glClear(GL_COLOR_BUFFER_BIT);
				menu->Init(windowManager, shaderManager, 0);
				currentState = menu;
				options->Cleanup();
			}
		}
		else
		if(currentState == finish)
		{
			if(Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_B)
			{
				menu->Init(windowManager, shaderManager, 0);
				currentState = menu;
				finish->Cleanup();
			}
			else
			if(Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_A)
			{
				demo->Init(windowManager, shaderManager, 0);
				currentState = demo;
				finish->Cleanup();
			}
		}*/
	}

	//prevState = Player1->GetState();
}

void Update(float dt)
{
	currentState->Update(deltaTime, fps);
	musicSelection = options->GetMusic();
	UpdateController();

	if(currentState->getQuit()) {
		currentState->SetSelection(0);
		currentState->Cleanup();
		keyboardSelect = false;
		exitBut = false;
		//soundBank->StopAllSounds();
		//soundBank->confirm.play();
		menu->Init(windowManager, shaderManager, 0);
		currentState = menu;
		return;
	}
	
	if(exitBut) {

		if(currentState == menu){
			quit();
			exitBut = false;
		}

	}

	if(currentState->GetSelection() == 2) {
		if(currentState == demo || currentState == multi) {
			//you win
			finish->AddTime(currentState->GetTime());
			currentState->SetSelection(0);
			currentState->Cleanup();
			keyboardSelect = false;
			exitBut = false;
			//soundBank->StopAllSounds();
			//soundBank->confirm.play();
			finish->Init(windowManager, shaderManager, 0);
			currentState = finish;
			return;
		}
		else if(currentState == finish) {
			currentState->SetSelection(0);
			currentState->Cleanup();
			keyboardSelect = false;
			//soundBank->StopAllSounds();
			//soundBank->confirm.play();
			menu->Init(windowManager, shaderManager, 0);
			currentState = menu;
			currentState->SetSelection(0);
		}
	}

	if(keyboardSelect) { //I'm afraid this is not dealt with in a unified manner, check the updatecontroller for more checks :(

		if (currentState == menu)
		{
			if ( /*(Player1->GetState().Gamepad.wButtons & XINPUT_GAMEPAD_A) || */keyboardSelect == true)
			{
				keyboardSelect = false;
				//soundBank->confirm.play();
				//if (currentState->GetSelection() == 0) //play state selection
				if(currentState->GetSelection() == 0){
					demo->Init(windowManager, shaderManager, options->GetMusic());
					currentState = demo;
					menu->Cleanup();
				}
				else
				if (currentState->GetSelection() == -1) // option state slection
				{
					multi->Init(windowManager, shaderManager, options->GetMusic());
					currentState = multi;
					menu->Cleanup();
				}
				else
				if (currentState->GetSelection() == -2) // quit selection
				{
					options->Init(windowManager, shaderManager, 0);
					currentState = options;
					menu->Cleanup();
					options->SetSelection(-3);
				}
				else
				if (currentState->GetSelection() == -3) // quit selection
				{
					quit();
				}
			}
		}
		else
		if (currentState == demo)
		{
			if (demo->GetSelection() == 1)
			{
				menu->Init(windowManager, shaderManager, 0);
				currentState = menu;
				demo->SetSelection(0);
				demo->Cleanup();
				//soundBank->StopAllSounds();
			}
		}
		else
		if (currentState == multi)
		{
			if (multi->GetSelection() == 1)
			{
				menu->Init(windowManager, shaderManager, 0);
				currentState = menu;
				multi->SetSelection(0);
				multi->Cleanup();
				//soundBank->StopAllSounds();
			}
		}
	

	}
}

void Render(void)
{
	//glClearColor(0.0f, 1.0f, 0.0f, 1.0f);
	//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glViewport( 0, 0, WIDTH, HEIGHT );
	
	currentState->Render(WIRE_FRAME);
	SDL_GL_SwapBuffers(); //web version of buffers swap
	//SDL_GL_SwapWindow(windowManager.GetWindow()); //swap buffers
}

void MainLoop() {
	//std::cout << "Hello loop : " << std::endl;

	SDL_Event sdlEvent; //variable to detect SDL events
	SDL_PollEvent(&sdlEvent);
	handleSDLEvent(sdlEvent);
	//RUNNING = handleSDLEvent(sdlEvent);
	UpdateTimeStep();
	Update(deltaTime);
	Render();
}

int main(int argc, char *argv[])
{
	/*SDL_GLContext glContext; //OpenGl context handle
	SDL_Window *hWindow; //window handle*/
	RUNNING = true; // variable to show game is running

	//hWindow = windowManager.SetupWebRC(glContext, WIDTH, HEIGHT, "Balls Drop!");
	screen = windowManager.SetupWebRC( WIDTH, HEIGHT );

	glewInit();
	GLenum valid;
	const GLubyte *version = glGetString( GL_VERSION );
	std::cout << "Found version of OpenGL : " << version << std::endl;
	Error::CheckGlewInitError(valid);
	//Init();

	//emscripten main loop call, use fps 0 to get RAF apprently
	emscripten_set_main_loop(MainLoop, 0, 0 );
	emscripten_set_main_loop_expected_blockers(10);
	emscripten_push_main_loop_blocker(Init, NULL);

	//emscripten_run_script("setTimeout(function() { window.close() }, 2000)");

	//SDL_DestroyWindow(hWindow);
	//SDL_GL_DeleteContext(glContext);
	//SDL_Quit();

	return 0;
}